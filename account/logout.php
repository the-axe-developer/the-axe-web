<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'].' - Account Logout';
if($_SESSION['user_status']!= 'login'){
    header('Location:'.$config['host'].'/account/login');
    exit();
} else {
    session_destroy();
    if(isset($_COOKIE['a45'])||isset($_COOKIE['a46'])||isset($_COOKIE['a47'])){
        unset($_COOKIE['a45']);
        unset($_COOKIE['a46']);
        unset($_COOKIE['a47']);
        setcookie('a45', '', time()-3600, '/');
        setcookie('a46', '', time()-3600, '/');
        setcookie('a47', '', time()-3600, '/');
    }
    echo '<script language="javascript">alert("Log out successfully!"); document.location="'.$config['host'].'";</script>';
    exit();
}
?>