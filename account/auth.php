<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Authenticate';
if(isset($_COOKIE['a45'])||isset($_COOKIE['a46'])||isset($_COOKIE['a47'])){
    $email  = base64_decode($_COOKIE['a45']);
    $uid    = base64_decode($_COOKIE['a46']);
    $logkey = $_COOKIE['a47'];
    $select = mysqli_query($conn, "SELECT * FROM `tbl_sessionkey` WHERE email='".$email."' AND logkey='".$logkey."'") or die(mysqli_error());
    if(mysqli_num_rows($select) > 0){
        $row = mysqli_fetch_assoc($select);
        if($row['id_user'] == $uid){
            $select2 = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='".$row['id_user']."'") or die(mysqli_error());
            $rows2   = mysqli_fetch_assoc($select2);
            if(mysqli_num_rows($select2) == 1){
                $_SESSION['user_id']        = $rows2['id_user'];
                $_SESSION['user_email']     = $rows2['email'];
                $_SESSION['user_status']    = "login";
                var_dump($_SESSION);
                header('Location:'.$config['host'].'/account/dashboard');
            }
        }
    }
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-sign-in-alt"></i> Login</div>
                        <div class="card-body">
                            <form method="POST" action="<?=$config['host'];?>/api/v1/userLogin" id="Login-Form">
                                <div id="result_submit"></div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" name="email" class="form-control" placeholder="Email Address" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary" id="button_id" type="submit"><i class="fas fa-sign-in-alt"></i> Login</button>
                            </form>
                        </div>
                    </div><br>
                </div>
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-user-plus"></i> Register</div>
                        <div class="card-body">
                            <div id="result_submit_2"></div>
                            <form method="POST" action="<?=$config['host'];?>/api/v1/userRegister" id="Register-Form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="f_name" placeholder="First Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="l_name" placeholder="Last Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" name="email" class="form-control" placeholder="Email Address" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="c_password" class="form-control" placeholder="Confirm Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-user-plus"></i> Register</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script src="https://www.google.com/recaptcha/api.js?render=<?=$config['recaptcha_site_key'];?>"></script>
        <script type="text/javascript">
            grecaptcha.ready(function() {
                grecaptcha.execute('<?=$config['recaptcha_site_key'];?>', {action: 'homepage'}).then(function(token){
                    $('#Register-Form').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Login-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fas fa-sign-in-alt"></i> Login');
                            if(hasil.result){
                                $("#result_submit").html('<div class="alert alert-success">'+hasil.content+'</div>');
                            } else {
                                $("#result_submit").html('<div class="alert alert-danger">'+hasil.content+'</div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fas fa-sign-in-alt"></i> Login');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("#result_submit_2").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
                $("form#Register-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-user-plus"></i> Register');
                            if(hasil.result){
                                $("#result_submit_2").html('<div class="alert alert-success">'+hasil.content+'</div>');
                            } else {
                                $("#result_submit_2").html('<div class="alert alert-danger">'+hasil.content+'</div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-user-plus"></i> Register');
                            $("#result_submit_2").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id_2").html('Loading..');
                            $("#result_submit_2").html('');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>