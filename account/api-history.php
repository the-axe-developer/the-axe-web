<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'API History';
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
if($_POST['aksi'] == 'Generate_API_Key'){
    $id_user = $_SESSION['user_id'];
    $apikey  = rand_str('all', '64');
    $update  = mysqli_query($conn, "UPDATE `tbl_user` SET apikey='$apikey' WHERE id_user='$id_user'") or die(mysqli_error($conn));
    unset($_POST);
    unlink($_POST);
    header('Location:'.$config['host'].'/account/dashboard');
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);  
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    include('../inc/navigation.phtml');
                    ?>
                </div>

                <div class="col-lg-6">

                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-chart-line"></i> Chart API Request</div>
                        <div class="card-body">
                        <?php
                        $today          = date('d-m-Y');
                        $harimundur1    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
                        $harimundur2    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d")-2, date("Y")));
                        $harimundur3    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d")-3, date("Y")));
                        $harimundur4    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d")-4, date("Y")));
                        $harimundur5    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
                        $hitungtoday    = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$today'"));
                        $hitungmundur1  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$harimundur1'"));
                        $hitungmundur2  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$harimundur2'"));
                        $hitungmundur3  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$harimundur3'"));
                        $hitungmundur4  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$harimundur4'"));
                        $hitungmundur5  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y') AS `date_format` FROM `tbl_api_history` WHERE FROM_UNIXTIME(tbl_api_history.created_at, '%d-%m-%Y')='$harimundur5'"));
                        ?>
                        <div class="chart">
                            <canvas id="chLine"></canvas>
                        </div>
                        </div>
                    </div><br>
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-fighter-jet"></i> Top API Request</div>
                        <div class="card-body">
                            <?php
                            $query = mysqli_query($conn, "SELECT `id_user`, `name`, COUNT(*) AS `name_count` FROM `tbl_api_history` WHERE `id_user`='$id_user' GROUP BY `name` ORDER BY `name_count` DESC") or die(mysqli_error($conn));
                            ?>
                            <div class="table-responsive">
                                <table class="display table table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>API Name</th>
                                            <th>Total Request</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(mysqli_num_rows($query) > 0){
                                    while($datax = mysqli_fetch_assoc($query)){ ?>
                                        <tr>
                                            <td><?=$datax['name'];?></td>
                                            <td><?=$datax['name_count'];?> Request</td>
                                        </tr>
                                    <?php }
                                    } else { ?>
                                        <tr>
                                            <td colspan="2" class="text-center">No data available</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><br>
                </div>

                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-history"></i> Request History</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="List-Data" class="display table table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>API Name</th>
                                            <th>IP Address</th>
                                            <th>Data</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>API Name</th>
                                            <th>IP Address</th>
                                            <th>Data</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div><br>
                </div>

            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
        <script type="text/javascript">
            var chartData = {
                labels: ["<?=$harimundur5;?>", "<?=$harimundur4;?>", "<?=$harimundur3;?>", "<?=$harimundur2;?>", "<?=$harimundur1;?>", "<?=$today;?>"],
                datasets: [{
                    data: [<?=$hitungmundur5;?>, <?=$hitungmundur4;?>, <?=$hitungmundur3;?>, <?=$hitungmundur2;?>, <?=$hitungmundur1;?>, <?=$hitungtoday;?>],
                }]
            };

            var chLine = document.getElementById("chLine");
            if(chLine){
                new Chart(chLine, {
                    type: 'line',
                    data: chartData,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }
                });
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#Navigation-Select").change(function() {
                    window.location.href = '<?=$config['host'];?>/'+$(this).val();
                });

                var table = $('#List-Data').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?=$config['host'];?>/api/v1/listApiHistory",
                    "order": [[0, 'desc']]
                });

            });
        </script>
    </body>
</html>