<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Dashboard';
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
if($_POST['aksi'] == 'Generate_API_Key'){
    $id_user = $_SESSION['user_id'];
    $apikey  = rand_str('all', '32');
    $update  = mysqli_query($conn, "UPDATE `tbl_user` SET apikey='$apikey' WHERE id_user='$id_user'") or die(mysqli_error($conn));
    unset($_POST);
    unlink($_POST);
    header('Location:'.$config['host'].'/account/dashboard');
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);  
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    include('../inc/navigation.phtml');
                    ?>
                </div>

                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-user"></i> Account Detail</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Account ID</th>
                                            <th>:</th>
                                            <td><?=$myakun['id_user'];?></td>
                                        </tr>
                                        <tr>
                                            <th>First Name</th>
                                            <th>:</th>
                                            <td><?=$myakun['first_name'];?></td>
                                        </tr>
                                        <tr>
                                            <th>Last Name</th>
                                            <th>:</th>
                                            <td><?=$myakun['last_name'];?></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <th>:</th>
                                            <td><?=$myakun['email'];?></td>
                                        </tr>
                                        <tr>
                                            <th>Register Date</th>
                                            <th>:</th>
                                            <td><?=date('Y-m-d H:i:s', $myakun['registered_date']);?></td>
                                        </tr>
                                        <tr>
                                            <th>Last Login</th>
                                            <th>:</th>
                                            <td><?=date('Y-m-d H:i:s', $myakun['last_login']);?></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div><br>
                </div>

                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-random"></i> API Key</div>
                        <div class="card-body">
                            <form action="" method="POST">
                                <?php if($myakun['apikey'] == null){ ?>
                                <input type="hidden" value="Generate_API_Key" name="aksi">
                                <button type="submit" class="btn btn-primary form-control" name="Generate_API_Key">Generate API Key</button>
                                <?php } else { ?>
                                <input type="hidden" value="Generate_API_Key" name="aksi">
                                <div class="form-group">
                                    <label><strong>Your API key</strong></label>
                                    <input type="text" name="apikey" class="form-control" value="<?=$myakun['apikey'];?>" disabled="true">
                                </div>
                                <button type="submit" class="btn btn-primary form-control">Update API Key</button>
                                <?php } ?>
                            </form>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#Navigation-Select").change(function() {
                    window.location.href = '<?=$config['host'];?>/'+$(this).val();
                });
            });
        </script>
    </body>
</html>