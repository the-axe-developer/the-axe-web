<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Manage Instagram';
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `id_user`='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);  
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    include('../../inc/navigation.phtml');
                    ?>
                </div>

                <div class="col-lg-12">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fab fa-instagram"></i> List Instagram</div>
                        <div class="card-body">
                        	<div class="form-group">
                        		<a class="btn btn-primary" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Add Data</a>
                        	</div>
                        	<div class="table-responsive">
                                <table id="List-Data" class="display table table-bordered" style="width:100%">
							        <thead>
							            <tr>
                                            <th>ID Instagram</th>
							                <th>Username</th>
							                <th>Added Date</th>
							                <th>Action</th>
							            </tr>
							        </thead>
							        <tfoot>
							            <tr>
							            	<th>ID Instagram</th>
							                <th>Username</th>
							            	<th>Added Date</th>
							                <th>Action</th>
							            </tr>
							        </tfoot>
							    </table>
							</div>
                        </div>
                    </div><br>
                </div>

                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View Account</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Useragent</label>
                                    <pre id="useragent_val"></pre>
                                </div>
                                <div class="form-group">
                                    <label>Cookies</label>
                                    <pre id="cookies_val"></pre>
                                </div>
                            </div>
                        </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

				<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
			        	<div class="modal-content">
			            	<div class="modal-header">
			              		<h4 class="modal-title">Add Account</h4>
			              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                		<span aria-hidden="true">&times;</span>
			              		</button>
			            	</div>
			            	<div class="modal-body">
                                <div class="alert alert-danger">
                                	Please verify your email and phone number in your instaram account
                                </div>
                                <div id="result_submit"></div>
                                <form role="form" method="POST" action="<?=$config['host'];?>/api/v1/addInstagramLogin.php" autocomplete="off" id="Login-Form">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username Instagram">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password Instagram">
                                    </div>
                                    <div class="form-group form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                        <label class="form-check-label" for="defaultCheck1">Saya setuju dengan <a href="terms" target="_blank">Ketentuan Layanan</a></label>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-paper-plane-o"></i> Sign In</button>
                                    </div>
                                </form>
                                <form role="form" method="POST" action="<?=$config['host'];?>/api/v1/addInstagramChallenge.php?step=1" autocomplete="off" id="Login-Checkpoint-1-Form">
                                    <div class="form-group">
                                        <select class="form-control" name="verifikasi" id="verifikasi">
                                            <option value="999">Pilih metode verifikasi</option>
                                            <option value="1">Email</option>
                                            <option value="0">Nomor Hp</option>
                                        </select>
                                    </div>
                                    <button type="submit" id="button_id" name="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Next Step</button>
                                </form>
                                <form role="form" method="POST" action="<?=$config['host'];?>/api/v1/addInstagramChallenge.php?step=2" autocomplete="off" id="Login-Checkpoint-2-Form">
                                    <div class="form-group">
                                        <input class="form-control" name="security_code" id="security_code" placeholder="Kode Verifikasi" type="text">
                                    </div>
                                    <button type="submit" id="button_id" name="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Verifikasi</button>
                                </form>
			            	</div>
			          	</div>
			          <!-- /.modal-content -->
			        </div>
			        <!-- /.modal-dialog -->
		      	</div>

            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $('#Login-Form').css('display', 'block');
            $('#Login-Checkpoint-1-Form').css('display', 'none');
            $('#Login-Checkpoint-2-Form').css('display', 'none');
            $(document).ready(function() {

                $("#Navigation-Select").change(function() {
                    window.location.href = '<?=$config['host'];?>/'+$(this).val();
                });

            	//Ambil Data
			    var table = $('#List-Data').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax": "<?=$config['host'];?>/api/v1/listInstagram",
			        "columnDefs": [ {
			            "targets": -1,
			            "data": null,
			            "defaultContent": "<div class='row'><div class='col-md-4'><button class='btn btn-primary btn-sm tblView'><i class='fa fa-eye'></i></button></div><div class='col-md-4'><button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></div></div>"
			        }],
                    "order": [[2, 'desc']]
			    });

			    //Tambahkan Data
			    $("form#Add-Data-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            	table.ajax.reload(null, false);
                            	$('#modalAdd').modal('hide');
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

			    //Tampilkan Form Ubah Data
				$('#List-Data tbody').on('click', '.tblView', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var id_data = data[0];
	                $.LoadingOverlay('show');
	                $.ajax({
	                    type : "POST",
	                    url  : "<?=$config['host'];?>/api/v1/getInstagram",
	                    data : {id:id_data},
	                    dataType : "JSON",
	                    success: function(result){
	                        $.LoadingOverlay('hide');
	                        $('#modalView').modal('show');
	                        $('#useragent_val').html(result.useragent);
	                        $('#cookies_val').html(result.cookies);
	                    },
                        error: function(a, b, c){
                            $.LoadingOverlay('hide');
                        }
	                });
	            });

                //Hapus Data
				$('#List-Data tbody').on('click', '.tblDelete', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var id_data = data[0];
	                swal({
			            title: "Are you sure?",
			            text: "You will not be able to recover this data!",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#DD6B55",
			            confirmButtonText: "Yes, delete it!",
			            closeOnConfirm: false
			        },
			        function(isConfirm){
			        	if(isConfirm){
                            $.LoadingOverlay('show');
				            $.ajax({
			                    type : "POST",
			                    url  : "<?=$config['host'];?>/api/v1/deleteInstagram",
			                    data : {id_data:id_data},
			                    dataType : "JSON",
			                    success: function(hasil){
                                    $.LoadingOverlay('hide');
		                            if(hasil.result){
		                            	swal("Success", ""+hasil.content+"", "success");
		                            	table.ajax.reload(null, false);
		                            } else
		                            	swal("Failed", ""+hasil.content+"", "error");
		                        },
                                error: function(a, b, c){
                                    $.LoadingOverlay('hide');
                                }
			                });
			          	} else {
			                swal("Cancelled", "Your imaginary file is safe :)", "error");
			        	} 
			       })
	            });

	            $("form#Login-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('Sign In');
                            if(hasil.result){
                                $("#result_submit").html('<div class="alert alert-success alert-dismissible"><b>Success!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                                table.ajax.reload(null, false);
                            } else {
                                if(hasil.cekpoint == 1){
                                    $('#Login-Form').css('display', 'none');
                                    $('#Login-Checkpoint-1-Form').css('display', 'block');
                                    $('#Login-Checkpoint-2-Form').css('display', 'none');
                                }
                                $("#result_submit").html('<div class="alert alert-danger alert-dismissible"><b>Failed!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                            }
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-paper-plane-o"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Login-Checkpoint-1-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl  = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('Sign In');
                            if(hasil.result == true){
                                //window.location.replace(hasil.redirect);
                                $("#result_submit").html('<div class="alert alert-success alert-dismissible"><b>Success!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                                $('#Login-Form').css('display', 'none');
                                $('#Login-Checkpoint-1-Form').css('display', 'none');
                                $('#Login-Checkpoint-2-Form').css('display', 'block');
                            } else {
                                $("#result_submit").html('<div class="alert alert-danger alert-dismissible"><b>Failed!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-paper-plane-o"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Login-Checkpoint-2-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl  = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('Next Step');
                            if(hasil.result == true){
                                $("#result_submit").html('<div class="alert alert-success alert-dismissible"><b>Success!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                                table.ajax.reload(null, false);
                            } else {
                                $("#result_submit").html('<div class="alert alert-danger alert-dismissible"><b>Failed!</b> '+hasil.content+'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-paper-plane-o"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

			} );
        </script>
    </body>
</html>