<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Storyloop';
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);  
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">
            <div id='loading_wrap' style='position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;'>Loading, please wait.</div>

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    include('../../inc/navigation.phtml');
                    ?>
                </div>

                <div class="col-lg-12">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-flask"></i> List Storyloop Setting</div>
                        <div class="card-body">
                        	<div class="form-group">
                        		<a class="btn btn-primary" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Add Data</a>
                        	</div>
                        	<div class="table-responsive">
                                <table id="List-Data" class="display table table-bordered" style="width:100%">
							        <thead>
							            <tr>
                                            <th>ID Setting</th>
							                <th>Setting Name</th>
							                <th>Target</th>
							                <th>Answer Question</th>
                                            <th>Target Count</th>
                                            <th>Sleep 1</th>
                                            <th>Sleep 2</th>
							                <th>Action</th>
							            </tr>
							        </thead>
							        <tfoot>
							            <tr>
							            	<th>ID Setting</th>
                                            <th>Setting Name</th>
                                            <th>Target</th>
                                            <th>Answer Question</th>
                                            <th>Target Count</th>
                                            <th>Sleep 1</th>
                                            <th>Sleep 2</th>
                                            <th>Action</th>
							            </tr>
							        </tfoot>
							    </table>
							</div>
                        </div>
                    </div><br>
                </div>

                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
			        	<div class="modal-content">
			            	<div class="modal-header">
			              		<h4 class="modal-title">Edit Data</h4>
			              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                		<span aria-hidden="true">&times;</span>
			              		</button>
			            	</div>
			            	<div class="modal-body">
			            		<form method="POST" action="<?=$config['host'];?>/api/v1/editStoryloop" id="Add-Data-Form">
                                    <input type="hidden" name="id_data" id="id_data">
                                    <div class="form-group">
                                        <label>Preset Name</label>
                                        <input type="text" class="form-control" name="name" id="sl_name" placeholder="Preset Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Target Username</label>
                                        <input type="text" class="form-control" name="target" id="sl_target" placeholder="Username Instagram" value="" data-role="tagsinput">
                                    </div>
                                    <div class="form-group">
                                        <label>Story Answer Comment Text</label>
                                        <input type="text" class="form-control" name="answer" id="sl_answer" placeholder="Story Answer" data-role="tagsinput">
                                    </div>
                                    <div class="form-group">
                                        <label>Target Count (Get list account of target)</label>
                                        <input type="number" class="form-control" name="target_count" id="sl_target_count" placeholder="Target Count">
                                    </div>
                                    <div class="form-group">
                                        <label>Sleep 1 (Pause at view story)</label>
                                        <input type="number" class="form-control" name="sleep_1" id="sl_sleep_1" placeholder="Sleep 1">
                                    </div>
                                    <div class="form-group">
                                        <label>Sleep 2 (Pause at view story per user)</label>
                                        <input type="number" class="form-control" name="sleep_2" id="sl_sleep_2" placeholder="Sleep 2">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
			            	</div>
			          	</div>
			          <!-- /.modal-content -->
			        </div>
			        <!-- /.modal-dialog -->
		      	</div>
                <!-- /.modal -->
				<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
			        	<div class="modal-content">
			            	<div class="modal-header">
			              		<h4 class="modal-title">Add Data</h4>
			              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                		<span aria-hidden="true">&times;</span>
			              		</button>
			            	</div>
			            	<div class="modal-body">
                                <form method="POST" action="<?=$config['host'];?>/api/v1/addStoryloop" id="Add-Data-Form">
                                    <div class="form-group">
                                        <label>Preset Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Preset Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Target Username</label>
                                        <input type="text" class="form-control" name="target" placeholder="Username Instagram" data-role="tagsinput">
                                    </div>
                                    <div class="form-group">
                                        <label>Story Answer Comment Text</label>
                                        <input type="text" class="form-control" name="answer" placeholder="Story Answer" data-role="tagsinput">
                                    </div>
                                    <div class="form-group">
                                        <label>Target Count (Get list account of target)</label>
                                        <input type="number" class="form-control" name="target_count" placeholder="Target Count">
                                    </div>
                                    <div class="form-group">
                                        <label>Sleep 1 (Pause at view story)</label>
                                        <input type="number" class="form-control" name="sleep_1" placeholder="Sleep 1">
                                    </div>
                                    <div class="form-group">
                                        <label>Sleep 2 (Pause at view story per user)</label>
                                        <input type="number" class="form-control" name="sleep_2" placeholder="Sleep 2">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
			            	</div>
			          	</div>
			          <!-- /.modal-content -->
			        </div>
			        <!-- /.modal-dialog -->
		      	</div>

            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#Navigation-Select").change(function() {
                    window.location.href = '<?=$config['host'];?>/'+$(this).val();
                });

            	//Ambil Data
			    var table = $('#List-Data').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax": "<?=$config['host'];?>/api/v1/listStoryloop",
			        "columnDefs": [ {
			            "targets": -1,
			            "data": null,
			            "defaultContent": "<div class='row'><div class='col-lg-4'><button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button></div><div class='col-lg-4'><button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></div></div>"
			        }]
			    });

			    //Tambahkan Data
			    $("form#Add-Data-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            	table.ajax.reload(null, false);
                            	$('#modalAdd').modal('hide');
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

			    //Tampilkan Form Ubah Data
				$('#List-Data tbody').on('click', '.tblEdit', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var id_data = data[0];
	                $.LoadingOverlay('show');
	                $.ajax({
	                    type : "POST",
	                    url  : "<?=$config['host'];?>/api/v1/getStoryloop",
	                    data : {id:id_data},
	                    dataType : "JSON",
	                    success: function(result){
                            $.LoadingOverlay('hide');
	                        $('#modalEdit').modal('show');
	                        $('#id_data').val(result.id_storyloop);
	                        $('#sl_name').val(result.name);
                            $('#sl_target').tagsinput('add', result.target);
                            $('#sl_answer').tagsinput('add', result.answer);
	                        $('#sl_target_count').val(result.target_count);
                            $('#sl_sleep_1').val(result.sleep_1);
                            $('#sl_sleep_2').val(result.sleep_2);
	                    }
	                });
	            });

	            //Simpan ubah data
	            $("form#Edit-Data-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                            	swal("Success!", ""+hasil.content+"", "success");
                            	table.ajax.reload(null, false);
                            	$('#modalEdit').modal('hide');
                            } else
                            	swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                //Hapus Data
				$('#List-Data tbody').on('click', '.tblDelete', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var id_data = data[0];
	                swal({
			            title: "Are you sure?",
			            text: "You will not be able to recover this data!",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#DD6B55",
			            confirmButtonText: "Yes, delete it!",
			            closeOnConfirm: false
			        },
			        function(isConfirm){
			        	if(isConfirm){
                            $.LoadingOverlay('show');
				            $.ajax({
			                    type : "POST",
			                    url  : "<?=$config['host'];?>/api/v1/deleteStoryloop",
			                    data : {id_data:id_data},
			                    dataType : "JSON",
			                    success: function(hasil){
                                    $.LoadingOverlay('hide');
		                            if(hasil.result){
		                            	swal("Success", ""+hasil.content+"", "success");
		                            	table.ajax.reload(null, false);
		                            } else
		                            	swal("Failed", ""+hasil.content+"", "error");
		                        }
			                });
			          	} else {
			                swal("Cancelled", "Your imaginary file is safe :)", "error");
			        	} 
			       })
	            });

			} );
        </script>
    </body>
</html>