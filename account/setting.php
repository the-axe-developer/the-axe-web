<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Account Setting';
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);  
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    include('../inc/navigation.phtml');
                    ?>
                </div>

                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-edit"></i> Edit Profile</div>
                        <div class="card-body">
                            <form method="POST" action="<?=$config['host'];?>/api/v1/editProfile" id="Update-Profile">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" name="email" class="form-control" placeholder="Email Address" required="" value="<?=$myakun['email'];?>" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="number" name="phone" class="form-control" placeholder="Phone Number" value="<?=$myakun['phone'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="f_name" placeholder="First Name" value="<?=$myakun['first_name'];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="l_name" placeholder="Last Name" value="<?=$myakun['last_name'];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-venus-mars"></i></span>
                                            </div>
                                            <select name="gender" class="form-control">
                                                <option value="-">-- Select Gender --</option>
                                                <option value="F">Female</option>
                                                <option value="M">Male</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Birthday Date</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-child"></i></span>
                                            </div>
                                            <input type="date" name="birthday" class="form-control" placeholder="Birthday Date" value="<?=$myakun['birthday'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id_1" type="submit"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                </div>

                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-lock"></i> Change Password</div>
                        <div class="card-body">
                            <form method="POST" action="<?=$config['host'];?>/api/v1/editPassword" id="Change-Password">
                                <div class="form-group">
                                    <label>Current Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="o_password" class="form-control" placeholder="Current Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="n_password" class="form-control" placeholder="Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="c_password" class="form-control" placeholder="Confirm Password" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#Navigation-Select").change(function() {
                    window.location.href = '<?=$config['host'];?>/'+$(this).val();
                });

                $("form#Update-Profile").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fas fa-sign-in-alt"></i> Login');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else {
                                swal("Failed!", ""+hasil.content+"", "error");
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fas fa-sign-in-alt"></i> Login');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("#result_submit_2").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Change-Password").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-user-plus"></i> Register');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else {
                                swal("Failed!", ""+hasil.content+"", "error");
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-user-plus"></i> Register');
                            $("#result_submit_2").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id_2").html('Loading..');
                            $("#result_submit_2").html('');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

            });
        </script>
    </body>
</html>