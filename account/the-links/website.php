<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'The Links';
if ($_SESSION['user_status'] != 'login') {
    header('Location:' . $config['host'] . '/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);

if ($_GET['id']) {
    $slug       = mysqli_real_escape_string($conn, stripslashes($_GET['id']));
    $select     = mysqli_query($conn, "SELECT * FROM `tbl_link_web` WHERE `id_link_web`='$slug'");
    if (mysqli_num_rows($select) < 1) {
        echo 'Error: not found!';
        exit();
    }
    $data       = mysqli_fetch_assoc($select);
    $id_data    = $data['id_link_web'];
} else {
    echo 'Error!';
    exit();
}
?>
<!doctype html>
<html lang="en">
<?php
include('../../inc/head.phtml');
?>

<body class="body-bg">
    <?php
    include('../../inc/header.phtml');
    ?>
    <main role="main" class="container" style="padding-top: 100px">
        <div id='loading_wrap' style='position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;'>Loading, please wait.</div>

        <div class="row">
            <div class="col-lg-12">
                <?php
                include('../../inc/navigation.phtml');
                ?>
            </div>

            <div class="col-lg-4">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-cog"></i> Website Settings</div>
                    <div class="card-body">
                        <form method="POST" action="<?= $config['host']; ?>/api/v1/editLinkWebsite" id="Add-Data-Form">
                            <input type="hidden" name="id_data" id="id_data">

                            <div class="form-group">
                                <label>Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" id="sl_title" placeholder="Website Title" required="true">
                            </div>

                            <div class="form-group">
                                <label for="basic-url" class="form-label">Slug URL <span class="text-danger">*</span></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon3">https://links.theaxe.net/</span>
                                    <input type="text" class="form-control" name="slug" id="sl_slug" placeholder="Slug URL" required="true" aria-describedby="basic-addon3" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" id="sl_description" placeholder="Website Description"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Theme <span class="text-danger">*</span></label>
                                <select class="form-control" name="theme" id="id_theme">
                                    <option value="">-- Choose Theme --</option>
                                    <option value="cerulean">Cerulean</option>
                                    <option value="cosmo">Cosmo</option>
                                    <option value="cyborg">Cyborg</option>
                                    <option value="darkly">Darkly</option>
                                    <option value="flatly">Flatly</option>
                                    <option value="journal">Journal</option>
                                    <option value="litera">Litera</option>
                                    <option value="lumen">Lumen</option>
                                    <option value="lux">Lux</option>
                                    <option value="materia">Materia</option>
                                    <option value="minty">Minty</option>
                                    <option value="morph">Morph</option>
                                    <option value="pulse">Pulse</option>
                                    <option value="quartz">Quartz</option>
                                    <option value="sandstone">Sandstone</option>
                                    <option value="simplex">Simplex</option>
                                    <option value="sketchy">Sketchy</option>
                                    <option value="Slate">Slate</option>
                                    <option value="solar">Solar</option>
                                    <option value="spacelab">Space Lab</option>
                                    <option value="superhero">SUperhero</option>
                                    <option value="united">United</option>
                                    <option value="vapor">Vapor</option>
                                    <option value="yeti">Yeti</option>
                                    <option value="zephyr">Zephyr</option>
                                </select>
                            </div>

                            <hr />

                            <div class="form-group">
                                <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div><br>
            </div>

            <div class="col-lg-8">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-link"></i> List Links</div>
                    <div class="card-body">
                        <div class="form-group">
                            <a class="btn btn-success" target="_blank" href="<?= $config['host_links']; ?>/<?= $data['slug']; ?>"><i class="fas fa-eye"></i> View Website</a>
                            <a class="btn btn-primary" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Add New Link</a>
                        </div>
                        <div class="table-responsive">
                            <table id="List-Data" class="display table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="display: none;">ID Website</th>
                                        <th>Title</th>
                                        <th>Url</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="display: none;">ID Website</th>
                                        <th>Title</th>
                                        <th>Url</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div><br>
            </div>

            <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Link</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="<?= $config['host']; ?>/api/v1/editLink" id="Add-Data-Form">
                                <input type="hidden" name="id_data" id="id_data">
                                <input type="hidden" name="id_website" id="id_website" value="<?= $_GET['id']; ?>">
                                <div class="form-group">
                                    <label>Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" id="ed_title" placeholder="Title" required="true">
                                </div>
                                <div class="form-group">
                                    <label>URL/Link <span class="text-danger">*</span></label>
                                    <input type="url" class="form-control" name="url" id="ed_url" placeholder="URL" required="true">
                                </div>
                                <hr />
                                <div class="form-group">
                                    <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Link</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="<?= $config['host']; ?>/api/v1/addLink" id="Add-Data-Form">
                                <input type="hidden" name="id_website" id="id_website" value="<?= $_GET['id']; ?>">
                                <div class="form-group">
                                    <label>Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" required="true">
                                </div>
                                <div class="form-group">
                                    <label>URL/Link <span class="text-danger">*</span></label>
                                    <input type="url" class="form-control" name="url" id="url" placeholder="URL" required="true">
                                </div>
                                <hr />
                                <div class="form-group">
                                    <button type="submit" id="button_id" name="submit" class="form-control btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>

    </main>
    <!-- /.container -->
    <?php
    include('../../inc/footer.phtml');
    ?>
    <?php
    include('../../inc/foot.phtml');
    ?>
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#Navigation-Select").change(function() {
                window.location.href = '<?= $config['host']; ?>/' + $(this).val();
            });

            var id_data = '<?= $_GET['id']; ?>';
            $.LoadingOverlay('show');
            $.ajax({
                type: "POST",
                url: "<?= $config['host']; ?>/api/v1/getLinkWebsite",
                data: {
                    id: id_data
                },
                dataType: "JSON",
                success: function(result) {
                    $.LoadingOverlay('hide');
                    // $('#modalEdit').modal('show');
                    $('#id_data').val(result.id_link_web);
                    $('#sl_title').val(result.title);
                    $('#sl_slug').val(result.slug);
                    $('#sl_description').val(result.description);
                }
            });

            //Ambil Data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/v1/listLink?id_website=<?= $_GET['id']; ?>",
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "className": "text-nowrap",
                        "defaultContent": "<button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button>"
                    },
                    {
                        "targets": 0,
                        "className": "d-none"
                    },
                    {
                        "targets": 1,
                        "className": "text-nowrap"
                    },
                    {
                        "targets": 2,
                        "className": "text-nowrap",
                        "render": function(data, type, row) {
                            return "<a href='" + data + "' target='_blank'>" + data + "</a>";
                        }
                    },
                    {
                        "targets": 3,
                        "className": "text-nowrap"
                    }
                ]
            });

            //Tambahkan Data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            //Tampilkan Form Ubah Data
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.LoadingOverlay('show');
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/v1/getLink",
                    data: {
                        id: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $.LoadingOverlay('hide');
                        $('#modalEdit').modal('show');
                        $('#id_data').val(result.id_link);
                        $('#ed_title').val(result.title);
                        $('#ed_url').val(result.url);
                    }
                });
            });

            //Simpan ubah data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            //Hapus Data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.LoadingOverlay('show');
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/v1/deleteLink",
                                data: {
                                    id_data: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    $.LoadingOverlay('hide');
                                    if (hasil.result) {
                                        swal("Success", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>