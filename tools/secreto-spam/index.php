<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-user-secret"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <div class="alert alert-info">This is example of secreto url <a href="https://secreto.site/16167876">https://secreto.site/16167876</a> and <b>16167876</b> is secreto ID</div>
                            <div id="result_submit"></div>
                            <div class="alert alert-info">Messages delay up to 3-5 minutes</div>
                            <form method="POST" action="api.php" id="Basic-Form">
                            	<div class="form-group">
	                                <label>Secreto profile ID</label>
	                                <div class="input-group mb-3">
	                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span>
	                                    </div>
	                                    <input type="number" class="form-control" name="id" id="id" placeholder="Secreto ID" required="">
	                                </div>
	                            </div>
                                <div class="form-group">
                                    <label>Total Spam</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-list-ol"></i></span>
                                        </div>
                                        <input type="number" class="form-control" id="quantity" id="quantity" placeholder="Quantity" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea name="message" id="message" class="form-control"></textarea>
                                </div>
                            </form>
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="ekse();"><i class="far fa-paper-plane"></i> Submit</button>
                            </div>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">

            function fromServer(i){
                return new Promise((resolve, reject) => {
                    setTimeout(function(){
                        $.ajax({
                            url: "api.php",
                            data: $("form#Basic-Form").serialize(),
                            timeout: false,
                            type: 'POST',
                            dataType: 'JSON',
                            success: function(hasil){
                                $("input").removeAttr("disabled", "disabled");
                                $("button").removeAttr("disabled", "disabled");
                                $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                                var html = $("#result_submit").html();
                                if(hasil.status == 'ok'){
                                    $("#result_submit").html(html + '<div class="alert alert-success">'+hasil.content+'</div>');
                                } else if(hasil.status == 'error'){
                                    $("#result_submit").html(html + '<div class="alert alert-warning">'+hasil.content+'</div>');
                                }
                            },
                            error: function(a, b, c) {
                                $("input").removeAttr("disabled", "disabled");
                                $("button").removeAttr("disabled", "disabled");
                                $("#button_id").html('<<i class="far fa-paper-plane"></i> Submit');
                                $("#result_submit").html(c);
                            },
                            beforeSend: function() {
                                $("input").attr("disabled", "disabled");
                                $("#button_id").html('Loading..');
                                $("button").attr("disabled", "disabled");
                            }
                        });
                        resolve(true)
                    }, 1000)
                })
            }

            async function forLoopGood(quantity){
                 for(var i=0;i<quantity;i++){
                    await fromServer(i);
                    //$("#xur").append("<p class='good'>"+as[i]+"</p>");
                }
            }

            function ekse(){
                var looping = $("#quantity").val();
                var pdata   = $("form#Basic-Form").serialize();
                var message = $("#message").val();
                var user    = $("#id").val();
                $("#result_submit").html('');
                if(Object.entries(message).length == 0){
                    $("#result_submit").html('<div class="alert alert-danger">Message is required</div>');
                } else if(Object.entries(user).length == 0){
                    $("#result_submit").html('<div class="alert alert-danger">Secreto user ID is required</div>');
                } else if(looping > 10){
                    $("#result_submit").html('<div class="alert alert-danger">Maximum quantity is 10</div>');
                } else if(looping < 1){
                    $("#result_submit").html('<div class="alert alert-danger">Minimum quantity is 1</div>');
                } else {
                    $("button").attr("disabled", "disabled");
                    $("input").attr("disabled", "disabled");
                    $("#button_id").html('Loading..');
                    console.log(looping);
                    forLoopGood(looping);
                }
            }
        </script>
    </body>
</html>