<?php
require('../../lib/config.php');
$array 		= array();
if(isset($_POST['id'])){
	$secId 		= mysqli_real_escape_string($conn, stripslashes(trim($_POST['id'])));
	$secUrl     = 'https://secreto.site/en/'.$secId;
    $secMsg     = trim($_POST['message']);
    $url        = 'https://api.secreto.site/sendmsg';
    $data       = json_encode(array('id' => $secId, 'message' => $secMsg));
    $ua         = getUserAgent();
    $header     = array('Authority: api.secreto.site', 'Accept: */*', 'User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Mobile Safari/537.36', 'Content-Type: application/json', 'Origin: https://secreto.site', 'Sec-Fetch-Site: same-site', 'Sec-Fetch-Mode: cors', 'Sec-Fetch-Dest: empty', 'Referer: '.$secUrl, 'Accept-Language: en-US,en;q=0.9,id;q=0.8,ms;q=0.7');
    $exec       = basic_cURL($url, $ua, 0, $data, $header);
    $headers    = http_parse_headers($exec[0]);
    if($headers[0] == 'HTTP/2 200'){
		$array['status'] 	= 'ok';
		$array['content']	= $secUrl." => Success => Limit remain: ".$headers['x-ratelimit-remaining'];
    } else {
    	$array['status'] 	= 'error';
		$array['content']	= $secUrl." => Failed => Limit remain: ".$headers['x-ratelimit-remaining'];
    }
} else {
	$array['status'] 	= 'error';
	$array['content']	= 'Oops error, please contact admin';
}
print_r(json_encode($array));
?>