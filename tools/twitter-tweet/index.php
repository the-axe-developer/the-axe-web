<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-envelope"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <div id="result_submit"></div>
                            <form method="POST" action="api.php" id="Basic-Form">
                                <div class="form-group">
                                    <label>Tweet</label>
                                    <textarea name="tweet" id="tweet" class="form-control" placeholder="Tweet"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Reply Tweet</label>
                                    <input type="text" name="reply" id="reply" class="form-control" placeholder="...">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="axemedia@example.com">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Basic-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'HTML',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(''+hasil+'');
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>