<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-user-secret"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>IP Address</th>
                                            <th>:</th>
                                            <td id="val_ip">{{ IP_ADDRESS }}</td>
                                        </tr>
                                        <tr>
                                            <th>Internet Service Provider</th>
                                            <th>:</th>
                                            <td id="val_isp">{{ ISP }}</td>
                                        </tr>
                                        <tr>
                                            <th>Timezone</th>
                                            <th>:</th>
                                            <td id="val_timezone">{{ TIMEZONE }}</td>
                                        </tr>
                                        <tr>
                                            <th>Country</th>
                                            <th>:</th>
                                            <td id="val_country">{{ COUNTRY }}</td>
                                        </tr>
                                        <tr>
                                            <th>Region</th>
                                            <th>:</th>
                                            <td id="val_region">{{ REGION }}</td>
                                        </tr>
                                        <tr>
                                            <th>City</th>
                                            <th>:</th>
                                            <td id="val_city">{{ CITY }}</td>
                                        </tr>
                                        <tr>
                                            <th>Latitude</th>
                                            <th>:</th>
                                            <td id="val_lat">{{ LAT }}</td>
                                        </tr>
                                        <tr>
                                            <th>Longitude</th>
                                            <th>:</th>
                                            <td id="val_lon">{{ LON }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <iframe class="embed-responsive" id="val_maps" src="https://maps.google.com/maps?q=<?=$json['ip']['latitude'];?>,<?=$json['ip']['longitude'];?>&amp;output=embed"></iframe>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $.ajax({
                    url: "http://ip-api.com/json/",
                    timeout: false,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(hasil){
                        if(hasil.status == 'success'){
                            $("#val_ip").html(hasil.query);
                            $("#val_isp").html(hasil.isp);
                            $("#val_city").html(hasil.city);
                            $("#val_country").html(hasil.country);
                            $("#val_region").html(hasil.regionName);
                            $("#val_timezone").html(hasil.timezone);
                            $("#val_lat").html(hasil.lat);
                            $("#val_lon").html(hasil.lon);
                            $("#val_maps").attr("src", "https://maps.google.com/maps?q="+hasil.lat+","+hasil.lon+"&amp;output=embed");
                        } else if(hasil.status == 'error'){
                            $("#result_submit").html(html + '<div class="alert alert-warning">'+hasil.content+'</div>');
                        }
                    }
                });
            });
        </script>
    </body>
</html>