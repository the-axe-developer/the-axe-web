<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-8">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-camera"></i> <?=$config['pagename'];?></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/malioboro"><button class="btn btn-primary form-control">Malioboro</button></a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/breksi"><button class="btn btn-primary form-control">Tebing Breksi</button></a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/parangtritis"><button class="btn btn-primary form-control">Pantai Parang Tritis</button></a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/tugu"><button class="btn btn-primary form-control">Tugu</button></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/prambanan"><button class="btn btn-primary form-control">Candi Prambanan</button></<?=$config['host'];?>/tools/cctv-yogyakarta/a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/merapi"><button class="btn btn-primary form-control">Merapi</button></a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/kali-biru"><button class="btn btn-primary form-control">Kali Biru</button></a>
                                </div>
                                <div class="form-group">
                                    <a href="<?=$config['host'];?>/tools/cctv-yogyakarta/pathuk"><button class="btn btn-primary form-control">Gunung Pathuk</button></a>
                                </div>
                            </div>
                        </div>
                        <?php
                        if($_GET['loc']=='breksi'){
                            $name = 'Tebing Breksi';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewBreksi.stream';
                        } elseif($_GET['loc']=='prambanan'){
                            $name = 'Candi Prambanan';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewCandiPrambanan.stream';
                        } elseif($_GET['loc']=='merapi'){
                            $name = 'Gunung Merapi';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewGunungMerapi.stream';
                        } elseif($_GET['loc']=='pathuk'){
                            $name = 'Gunung Pathuk';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewGunungPathuk.stream';
                        } elseif($_GET['loc']=='kali-biru'){
                            $name = 'Kali Biru';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewKalibiru.stream';
                        } elseif($_GET['loc']=='malioboro'){
                            $name = 'Malioboro';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewMalioboro.stream';
                        } elseif($_GET['loc']=='parangtritis'){
                            $name = 'Pantai Parang Tritis';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewPantaiParangTritis.stream';
                        } elseif($_GET['loc']=='tugu'){
                            $name = 'Tugu';
                            $uri  = 'rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewTugu.stream';
                        } else {
                            $name = 'Error';
                        }
                        if($_GET['loc']){ ?>
                            <h4>Lokasi CCTV : <?=$name;?></h4>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://static.u-pro.fr/vod/static/upro/player/uplayer/107/u-player.html?prot=rtmp&netConnection=<?=$uri;?>&source=<?=$uri;?>&live=1&chapter=disable&autoplay=0&scrubber=1&play=1&fullscreen=1&volume=1&time=1&bgColor=ffffff&img=http://static.u-pro.fr/vod/static/upro/img/fond_player_upro.png&controlColor=250,84,0,0.5&controlGradient=none&leftSliderColor=ffffff&rightSliderColor=158CBA&bufferSliderColor=d4d4d4&timeTextColor=ffffff&durationTextColor=333333&bgTooltipColor=158CBA&textTooltipColor=333333&buttonOverColor=ffffff" width="620" height="349" border="0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe>
                            </div>
                            <p>Video Source <b><a href="http://mam.jogjaprov.go.id">Multimedia Data Center dan Media Asset Management Yogyakarta</a></b></p>
                        <?php } else { ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe src="https://static.u-pro.fr/vod/static/upro/player/uplayer/107/u-player.html?prot=rtmp&netConnection=rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewBreksi.stream&source=rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewBreksi.stream&live=1&chapter=disable&autoplay=0&scrubber=1&play=1&fullscreen=1&volume=1&time=1&bgColor=ffffff&img=http://static.u-pro.fr/vod/static/upro/img/fond_player_upro.png&controlColor=250,84,0,0.5&controlGradient=none&leftSliderColor=ffffff&rightSliderColor=158CBA&bufferSliderColor=d4d4d4&timeTextColor=ffffff&durationTextColor=333333&bgTooltipColor=158CBA&textTooltipColor=333333&buttonOverColor=ffffff" width="620" height="349" border="0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe src="https://static.u-pro.fr/vod/static/upro/player/uplayer/107/u-player.html?prot=rtmp&netConnection=rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewMalioboro.stream&source=rtmp://mam.jogjaprov.go.id:1935/cctv-public/ViewMalioboro.stream&live=1&chapter=disable&autoplay=0&scrubber=1&play=1&fullscreen=1&volume=1&time=1&bgColor=ffffff&img=http://static.u-pro.fr/vod/static/upro/img/fond_player_upro.png&controlColor=250,84,0,0.5&controlGradient=none&leftSliderColor=ffffff&rightSliderColor=158CBA&bufferSliderColor=d4d4d4&timeTextColor=ffffff&durationTextColor=333333&bgTooltipColor=158CBA&textTooltipColor=333333&buttonOverColor=ffffff" width="620" height="349" border="0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-4">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                    <div class="card-body text-center">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                </div><br>
            </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
    </body>
</html>