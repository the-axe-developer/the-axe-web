<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-quote-left"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <?php
                            $url    = 'https://andruxnet-random-famous-quotes.p.rapidapi.com/?cat=famous&count=10';
                            $key    = $config['rapidapi_key'];
                            $exc    = basic_cURL($url, 0, 0, 0, array('X-Mashape-Key: '.$key, 'X-RapidAPI-Host: andruxnet-random-famous-quotes.p.rapidapi.com'), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
                            $json   = json_decode($exc[1]);
                            foreach ($json as $json) {    
                            ?>
                            <blockquote class="blockquote">
                              <p class="mb-0"><?php echo $json->quote; ?></p>
                              <footer class="blockquote-footer"><?php echo $json->author; ?></footer>
                            </blockquote>
                            <?php } ?>
                            <br>
                            <button class="btn btn-sm btn-primary" id="ref_butn">Refresh the page!</button>
                        </div>
                    </div><br>
                </div>
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#ref_butn").click(function(){
                    location.reload();
                });
            });
        </script>
    </body>
</html>