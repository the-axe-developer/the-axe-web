<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE `page_name`='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-quote-left"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <div id="result_submit"></div>
                            <form method="POST" action="test.php" id="Basic-Form">
                                <div class="form-group">
                                    <label>Quotes Text</label>
                                    <textarea class="form-control" name="quotes" placeholder="Quotes here..."></textarea>
                                </div>
                            	<div class="form-group">
	                                <label>Instagram Username</label>
	                                <div class="input-group mb-3">
	                                    <div class="input-group-prepend"><span class="input-group-text">@</span></div>
	                                    <input type="text" class="form-control" name="instagram" placeholder="Enter your instagram username" required="">
	                                </div>
	                            </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-info"></i></i> Info</div>
                        <div class="card-body">
                            <p>Don't forget to follow our instagram <a href="https://instagram.com/axequotebot">@axequotebot</a></p>
                            <img src="https://res.cloudinary.com/ddw14mjcm/image/upload/v1603433705/Screenshot_2020-10-23-13-13-58-35_rtxmu5.jpg" class="img-fluid">
                        </div>
                    </div><br>
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Basic-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            if(hasil.result == 1){
                            	$("#result_submit").html('<div class="alert alert-success">'+hasil.content+'</div>');
                                $("#result_submit").html($("#result_submit").html()+''+hasil.image+'</div><br>');
                            } else {
                            	$("#result_submit").html('<div class="alert alert-warning">'+hasil.content+'</div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>