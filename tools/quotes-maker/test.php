<?php
require('../../lib/config.php');
require('img.php');

$useragent 	= 'Instagram 35.0.0.20.96 Android (10/1.5.3; 160; 1080x1920; samsung; SM-N9000; SM-N9000; smdkc210; en_US)';
$cookies 	= 'ds_user=axequotebot;csrftoken=cC0jPZOl7BWv6Ga3zfrflFSvs3Ad417V;rur=PRN;mid=X5Jg6gABAAGVLNaZw8aFJK8104iL;ds_user_id=6251732453;urlgen="{\"36.79.250.11\": 7713}:1kVp1Q:F5RLRjI2kDfF8Q01ADt4Z9JU6mc";sessionid=6251732453%3AUkPs4GqYKIcxHu%3A29;';

function upload_photo($useragent, $cookies, $photo, $caption)
{
    global $config;
    $array          = array();
    $binary_file    = $photo;
    $upload_id      = number_format(round(microtime(true) * 1000), 0, '', '');
    $url            = 'https://i.instagram.com/rupload_igphoto/%s_%d_%d';
    $url            = sprintf($url, $upload_id, 0, hash_code($binary_file));
    $filedata       = file_get_contents($binary_file);
    $upload_params  = 
    [
        'upload_id'         => (string) $upload_id,
        'retry_context'     => json_encode([
            'num_step_auto_retry'   => 0,
            'num_reupload'          => 0,
            'num_step_manual_retry' => 0,
        ]),
        'image_compression' => '{"lib_name":"moz","lib_version":"3.1.m","quality":"87"}',
        'xsharing_user_ids' => json_encode([]),
        'media_type'        => '1',
    ];
    $upload_header  = 
    array(
        'X_FB_PHOTO_WATERFALL_ID: '.generate_guid(),
        'X-Instagram-Rupload-Params: '.json_encode(reorder_by_hash_code($upload_params)),
        'X-Entity-Type: image/jpeg',
        'X-Entity-Name: '.$binary_file,
        'X-Entity-Length: '.strlen($filedata),
        'Offset: 0'
    );
    $upload = proccess(0, $useragent, $url, $cookies, $filedata, $upload_header, $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $upload = json_decode($upload[1], true);
    if($upload['status'] == 'ok'){
        $url        = 'media/configure/?timezone_offset='.date('Z');
        $date       = date('Y:m:d H:i:s');
        $postdata   = 
        [
            'upload_id' => $upload_id,
            'date_time_original' => $date,
            'date_time_digitalized' => $date,
            'caption' => $caption,
            'source_type' => '4',
            'media_folder' => 'Camera'
        ];
        $configure = proccess(1, $useragent, $url, $cookies, $postdata, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
        $configure = json_decode($configure[1], true);
        if($configure['status'] == 'ok'){
            $array['result']    = 1;
            $array['content']   = 'Success upload photo to <a href="https://instagram.com/p/'.$configure['media']['code'].'">https://instagram.com/p/'.$configure['media']['code'].'</a> Don\'t forget to follow <a href="https://instagram.com/axequotebot">@axequotebot</a> on instagram</div>';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed to configure photo';
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'Failed to upload photo';
    }

    return $array;
}

function get_bg_image(){
	$url = 'https://picsum.photos/640/640/?random';
	$ch  = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$a    = curl_exec($ch);
	$url  = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); 

	return $url;
}

$badword 	= basic_cURL($config['host'].'/api/v2/bad-words/?apikey='.$config['axemedia_key'].'&text='.urlencode($_POST['quotes']));
$badword 	= json_decode($badword[1], true);
$checkuser 	= get_user_id($_POST['instagram']);
if(empty($_POST['quotes'])){
	$array['result']    = 0;
   	$array['content']   = 'Quotes is required';
} else if(empty($_POST['instagram'])){
	$array['result']    = 0;
   	$array['content']   = 'Instagram username is required';
} else if(strlen($_POST['quotes']) < 15){
	$array['result']    = 0;
   	$array['content']   = 'Minimum quote character length is 15';
} else if($badword['data']['bad'] == true){
	$array['result']    = 0;
   	$array['content']   = 'Your quotes contains bad word';
} else {
	$capt       = "Kiriman quote dari @".strtolower($_POST['instagram'])."\n\nBikin quote sendiri yuk!\nDengan cara klik tautan di bio ya..";
	$quote		= $_POST['quotes'];
	$bg 		= get_bg_image();
	$overlay 	= 'files/overlay.png';
	$gelap 		= 'files/gelap.png';
	$font  		= 'files/Ubuntu-Medium.ttf';
	$filename  	= md5(rand(000,9999)).'.png';
	$filejpg   	= md5(rand(000,9999)).'.jpg';
	$image 		= new PHPImage();
	$image->setQuality(10);
	$image->setDimensionsFromImage($overlay);
	$image->draw(''.$bg.'');
	$image->draw('files/gelap.png');
	$image->draw($overlay, '50%', '75%');
	$image->setFont($font);
	$image->setTextColor(array(255, 255, 255));
	$image->setAlignVertical('center');
	$image->setAlignHorizontal('center');
	$image->textBox($quote, array('fontSize' => 28, 'x' => 130, 'y' => 210, 'width' => 380, 'height' => 200, 'debug' => false));
	$image->save($filename);
	$imagebase64 = base64_encode(file_get_contents($filename));
	$pngtojpg    = png2jpg($filename, $filejpg, 100);
	$pesan      .= "<a href='data:image/png;base64,".$imagebase64."' target='_blank' download='$filename'><img class='img-fluid' src='data:image/png;base64,".$imagebase64."'/></a>";
// 	$array = upload_photo($useragent, $cookies, $filejpg, $capt);
	$array['content'] = $pesan;
	unlink($filename);
	unlink($filejpg.'.jpg');
}
print_r(json_encode($array));
?>