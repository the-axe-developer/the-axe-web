<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-link"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <div id="result_submit"></div>
                            <form method="POST" action="api.php" id="Basic-Form">
                                <div class="form-group">
                                    <label>Link or URL</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-link"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="link" placeholder="Enter link or url" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                                </div>
                            </form>
                            <div class="card" id="link-preview" style="display: none;">
                                <img class="card-img-top" id="link_img" src="..." alt="OG Image">
                                <div class="card-body">
                                    <h5 class="card-title" id="link_title"></h5>
                                    <p class="card-text" id="link_desc"></p>
                                    <a href="" id="link_url" class="card-link"><span id="link_url_2"></span></a>
                                </div>
                            </div>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Basic-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            if(hasil.status == 'ok'){
                                //$("#result_submit").html('<div class="alert alert-success">'+hasil.content+'</div>');
                                $("#link-preview").show();
                                $("#link_title").html(hasil.data.title);
                                $("#link_desc").html(hasil.data.description);
                                $("#link_url_2").html(hasil.data.url);
                                $("#link_url").attr("href", hasil.data.url);
                                $("#link_img").attr("src", hasil.data.image);
                            } else if(hasil.status == 'error'){
                                $("#result_submit").html('<div class="alert alert-warning">'+hasil.data.description+'</div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("#link-preview").hide();
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
        </script>
    </body>
</html>