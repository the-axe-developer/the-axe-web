<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fab fa-instagram"></i> <?=$config['pagename'];?></div>
                    <div class="card-body">
                        <form method="POST" action="api.php" id="Basic-Form">
                            <div class="form-group">
                                <label>Username (without @)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="axemedia" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                            </div>
                        </form>
                        <hr>
                        <div id="result_submit"></div>
                    	<div id="social-page" style="display: none;">
                    		<div class="row">
                    			<div class="col-md-12 mx-auto text-center">
                    				<img id="ig-image" src="" class="img-fluid rounded-circle" alt="Responsive image">
                    			</div>
                                <div class="col-md-12 mx-auto text-center">
                                    <strong><span id="ig-name" class="">0</span></strong><br><br>
                                </div>
                    		</div><br>
                    		<div class="row text-center">
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Posts</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-post">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Follower</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-follower">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Following</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-following">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <td id="ig-name">0</td>
                                            </tr>
                                            <tr>
                                                <th>Biography</th>
                                                <td id="ig-bio">0</td>
                                            </tr>
                                            <tr>
                                                <th>Is Private</th>
                                                <td id="ig-private">0</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                    		</div>
                    	</div>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                    <div class="card-body text-center">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                </div><br>
            </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){

            	function animateValue(id, start, end, duration) {
				    var range = end - start;
				    var current = start;
				    var increment = end > start? 1 : -1;
				    var stepTime = Math.abs(Math.floor(duration / range));
				    var obj = document.getElementById(id);
				    var timer = setInterval(function() {
				        current += increment;
				        obj.innerHTML = current;
				        if (current == end) {
				            clearInterval(timer);
				        }
				    }, stepTime);
				}

				function formatNumber(num) {
					return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
				}

                $("form#Basic-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            if(hasil.status == 'ok'){
                            	$("#social-page").show();
                                $("#ig-post").html(formatNumber(hasil.user.media_count));
                                $("#ig-follower").html(formatNumber(hasil.user.follower_count));
                                $("#ig-following").html(formatNumber(hasil.user.following_count));
                                $("#ig-name").html(hasil.user.full_name);
                                $("#ig-bio").html(hasil.user.biography);
                                $("#ig-image").attr("src", hasil.user.profile_pic_url);
                                $("#ig-image").attr("alt", "Profile Image");
                            } else if(hasil.status == 'error'){
                                $("#result_submit").html('<div class="alert alert-danger"><span>'+hasil.message+'</span></div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                        	$("#social-page").hide();
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>