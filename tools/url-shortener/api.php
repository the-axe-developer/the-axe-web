<?php
require('../../lib/config.php');
$array 		= array();
$url 		= mysqli_real_escape_string($conn, stripslashes(trim($_POST['url'])));
$type 		= mysqli_real_escape_string($conn, stripslashes(trim($_POST['type'])));
if(isset($_POST['url'])){
	if($type == 'ReLink'){
		$data		= 'url='.$url;
		$html 		= basic_cURL('https://rel.ink/api/links/', getUserAgent(), 0, $data, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
		$data 		= json_decode($html[1], true);
		if($data['hashid']){
			$array['status'] 	= 'ok';
			$array['content']	= '<a href="https://rel.ink/'.$data['hashid'].'">https://rel.ink/'.$data['hashid'].'</a>';
		} else {
			$array['status'] 	= 'error';
			$array['content']	= 'Oops error, please contact admin';
		}
	} else if($type == 'CleanURI'){
		$data		= 'url='.$url;
		$html 		= basic_cURL('https://cleanuri.com/api/v1/shorten', getUserAgent(), 0, $data, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
		$data 		= json_decode($html[1], true);
		if($data['result_url']){
			$array['status'] 	= 'ok';
			$array['content']	= '<a href='.$data['result_url'].'>'.$data['result_url'].'</a>';
		} else {
			$array['status'] 	= 'error';
			$array['content']	= 'Oops error, please contact admin';
		}
	} else {
		$array['status'] 	= 'error';
		$array['content']	= 'Oops error, please select service';
	}
} else {
	$array['status'] 	= 'error';
	$array['content']	= 'Oops error, please contact admin';
}
print_r(json_encode($array));
?>