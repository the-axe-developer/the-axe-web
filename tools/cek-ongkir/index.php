<?php
session_start();
require('../../lib/config.php');
require('class.ongkir.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if ($tools_data['status'] == 'OFF') {
    header("Location: " . $config['host'] . "/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
<?php
include('../../inc/head.phtml');
?>

<body class="body-bg">
    <?php
    include('../../inc/header.phtml');
    ?>
    <main role="main" class="container" style="padding-top: 100px">

        <div class="row">
            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-truck"></i> <?= $config['pagename']; ?></div>
                    <div class="card-body">
                        <div id="result_submit"></div>
                        <form method="POST" action="cek().php" id="Basic-Form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label><b>Propinsi Awal</b></label>
                                        <select name="propinsi" id="propinsi" class="form-control">
                                            <option value="">-- Pilih Propinsi --</option>
                                            <?php
                                            $init = new RajaOngkir('99ccff6a4c3f3eca2ef0b69176ed7fd8', true);
                                            $data = json_decode($init->getProvince(), true);
                                            foreach ($data as $item) {
                                            ?>
                                                <option value="<?= $item['province_id']; ?>"><?= $item['province']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><b>Kota/Kabupaten Awal</b></label>
                                        <select name="kabupaten" id="kabupaten" class="form-control">
                                            <option value="">--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label><b>Propinsi Tujuan</b></label>
                                        <select name="propinsi2" id="propinsi2" class="form-control">
                                            <option value="">-- Pilih Propinsi --</option>
                                            <?php
                                            $init = new RajaOngkir('99ccff6a4c3f3eca2ef0b69176ed7fd8', true);
                                            $data = json_decode($init->getProvince(), true);
                                            foreach ($data as $item) {
                                            ?>
                                                <option value="<?= $item['province_id']; ?>"><?= $item['province']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><b>Kota/Kabupaten Tujuan</b></label>
                                        <select name="kabupaten2" id="kabupaten2" class="form-control">
                                            <option value="">--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><b>Berat (Gram)</b></label>
                                <input type="text" class="form-control" placeholder="Berat" id="berat" name="berat" value="1000">
                            </div>
                            <div class="form-group">
                                <label><b>Kurir</b></label>
                                <select name="kurir" id="kurir" class="form-control">
                                    <option value="jne">JNE</option>
                                    <option value="pos">Pos</option>
                                    <option value="tiki">Tiki</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                    <div class="card-body text-center">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                </div><br>
            </div>
        </div>

    </main>
    <!-- /.container -->
    <?php
    include('../../inc/footer.phtml');
    ?>
    <?php
    include('../../inc/foot.phtml');
    ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.remote.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#kabupaten").remoteChained({
                parents: "#propinsi",
                url: "api.php",
                loading: "Memuat..."
            });
        });
        $(document).ready(function() {
            $("#kabupaten2").remoteChained({
                parents: "#propinsi2",
                url: "api.php",
                loading: "Memuat..."
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Basic-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'HTML',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                        $("#result_submit").html('' + hasil + '');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>