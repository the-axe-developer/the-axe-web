<?php
require_once('../../lib/config.php');
require('class.ongkir.php');
$init = new RajaOngkir('99ccff6a4c3f3eca2ef0b69176ed7fd8', true);
if ($_POST['kabupaten'] || $_POST['kabupaten2'] || $_POST['berat'] || $_POST['kurir']) {
  $data = json_decode($init->getCost($_POST['kabupaten'], $_POST['kabupaten2'], $_POST['berat'], $_POST['kurir']), true);
?>
  <table class="table table-responsive">
    <tbody>
      <tr>
        <td><b>Asal</b></td>
        <td><b>:</b></td>
        <td><?= $data['rajaongkir']['origin_details']['type']; ?> <?= $data['rajaongkir']['origin_details']['city_name']; ?></td>
      </tr>
      <tr>
        <td><b>Tujuan</b></td>
        <td><b>:</b></td>
        <td><?= $data['rajaongkir']['destination_details']['type']; ?> <?= $data['rajaongkir']['destination_details']['city_name']; ?></td>
      </tr>
    </tbody>
  </table>

  <table class="table table-responsive">
    <thead>
      <tr class="text-center">
        <th><b>Layanan</b></th>
        <th><b>Deskripsi</b></th>
        <th><b>Biaya</b></th>
        <th style="white-space: nowrap;"><b>Estimasi Sampai</b></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($data['rajaongkir']['results'][0]['costs'] as $val) { ?>
        <tr>
          <td style="white-space: nowrap;"><?= $val['service']; ?></td>
          <td style="white-space: nowrap;"><?= $val['description']; ?></td>
          <td style="white-space: nowrap;"><?= rupiah($val['cost'][0]['value']); ?></td>
          <td style="white-space: nowrap;"><?= $val['cost'][0]['etd']; ?> Hari</td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

<?php
}
?>