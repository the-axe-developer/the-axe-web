<?php
require('../../lib/config.php');
if($_POST['email']){
    $email      = trim($_POST['email']);
    $l          = strlen($email); 
    if($l > 30){ 
        echo "<div class='alert alert-danger'>Maksimum email length 30.</alert>";
    } else {
        echo "<br><textarea class='form-control'>";
        $total 		= pow(2,$l-1);
        $email_arr	= str_split($email);
        for($i=0; $i<$total; $i++){
            $w_arr	= $email_arr;
            $bits	= $i;
            $pos 	= 0;
            while($bits){
                if($bits&1){
                    $w_arr[$pos].=".";
                }
                $bits=$bits>>1;
                $pos++;
            }
            echo implode($w_arr).$add."".PHP_EOL;
        }
        echo "</textarea><br>";
    }
}
?>