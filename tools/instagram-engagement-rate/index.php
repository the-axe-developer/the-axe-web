<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fab fa-instagram"></i> <?=$config['pagename'];?></div>
                    <div class="card-body">
                        <form method="POST" action="api.php" id="Basic-Form">
                            <div class="form-group">
                                <label>Username (without @)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="axemedia" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="button_id" type="submit"><i class="far fa-paper-plane"></i> Submit</button>
                            </div>
                        </form>
                        <hr>
                        <div id="result_submit"></div>
                    	<div id="social-page" style="display: none;">
                    		<div class="row">
                    			<div class="col-md-12 mx-auto text-center">
                    				<img id="ig-image" src="" class="img-fluid rounded-circle" alt="Responsive image">
                    			</div><br>
                                <div class="col-md-12 mx-auto text-center">
                                    <strong><span id="ig-name" class="">0</span></strong><br>
                                </div>
                    		</div><br>
                    		<div class="row text-center">
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Posts</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-post">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Follower</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-follower">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Following</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-following">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Average Likes</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-av-likes">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Average Comments</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-av-comments">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Average Interaction</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-av-interaction">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    			<div class="col-md-12">
                    				<div class="row">
                    					<div class="col-md-12">
                    						<span><strong><h5>Engagement Rate</h5></strong></span>
                    					</div>
                    					<div class="col-md-12">
                    						<strong><span id="ig-engagement-rate">0</span><span id="ig-engagement-unit">0</span></strong><br><br>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                    <div class="card-body text-center">
                    	<div class="table-responsive">
                    		<table class="table table-bordered">
                    			<thead>
                    				<tr>
                    					<th>Followers Range</th>
                    					<th>Average Engagment Rate</th>
                    				</tr>
                    			</thead>
                    			<tbody>
                    				<tr>
                					    <td>&lt; 5K</td>
                                    	<td><strong>7.27%</strong></td>
                               	 	</tr>
                                    <tr><td>5K - 10K</td>
                                    	<td><strong>5.58%</strong></td>
                                	</tr>
                                    <tr><td>10K - 25K</td>
                                    	<td><strong>4.66%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>25K - 50K</td><td><strong>3.83%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>50K - 100K</td>
                                    	<td><strong>3.22%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>100K - 250K</td>
                                    	<td><strong>3.02%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>250K - 500K</td>
                                    	<td><strong>3.23%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>500K - 1m</td>
                                    	<td><strong>2.98%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>1m - 2.5m</td>
                                    	<td><strong>2.59%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>2.5m - 5m</td>
                                    	<td><strong>1.89%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td>5m - 10m</td>
                                    	<td><strong>0.99%</strong></td>
                                	</tr>
                                    <tr>
                                    	<td> &gt; 10m</td>
                                    	<td><strong>0.67%</strong></td>
                                	</tr>
                                </tbody>
                    		</table>
                    	</div>
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                </div><br>
            </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script type="text/javascript">
            $(document).ready(function(){

            	function animateValue(id, start, end, duration) {
				    var range = end - start;
				    var current = start;
				    var increment = end > start? 1 : -1;
				    var stepTime = Math.abs(Math.floor(duration / range));
				    var obj = document.getElementById(id);
				    var timer = setInterval(function() {
				        current += increment;
				        obj.innerHTML = current;
				        if (current == end) {
				            clearInterval(timer);
				        }
				    }, stepTime);
				}

				function formatNumber(num) {
					return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
				}

                $("form#Basic-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            if(hasil.status == 'ok'){
                            	$("#social-page").show();
                            	$("#ig-name").html(hasil.data.full_name);
                                $("#ig-post").html(formatNumber(hasil.data.media_count));
                                $("#ig-follower").html(formatNumber(hasil.data.follower_count));
                                $("#ig-following").html(formatNumber(hasil.data.following_count));
                                $("#ig-av-likes").html(formatNumber(hasil.data.average.average_likes));
                                $("#ig-av-comments").html(formatNumber(hasil.data.average.average_comments));
                                $("#ig-av-interaction").html(formatNumber(hasil.data.average.average_interaction));
                                $("#ig-engagement-rate").html(hasil.data.engagement.engagement_rate);
                                $("#ig-engagement-unit").html(hasil.data.engagement.engagement_unit);
                                $("#ig-image").attr("src", hasil.data.profile_pic_url);
                                $("#ig-image").attr("alt", "Profile Image");
                            } else if(hasil.status == 'error'){
                                $("#result_submit").html('<div class="alert alert-danger"><span>'+hasil.message+'</span></div>');
                            }
                        },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="far fa-paper-plane"></i> Submit');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                        	$("#social-page").hide();
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>