<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Main Page';
$tools_name         = $_SERVER['REQUEST_URI'];
$tools_name         = explode('tools/', $tools_name);
$tools_name         = explode('/', $tools_name[1]);
$tools_name         = $tools_name[0];           
$tools_cek          = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE page_name='$tools_name'");
$tools_data         = mysqli_fetch_array($tools_cek);
$config['pagename'] = $tools_data['name'];
if($tools_data['status'] == 'OFF'){
    header("Location: ".$config['host']."/error/404");
    exit();
}
?>
<!doctype html>
<html lang="en">
    <?php
    include('../../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-mobile-alt"></i> <?=$config['pagename'];?></div>
                        <div class="card-body">
                            <iframe src="<?=$config['host']?>/tools/apps-screenshot/Apps-Screenshot/index.html" style="border:none;width:100%" height="1000"></iframe>
                            <p>Thanks to <b><a href="http://bachors.com">bachors.com</a></b> for source code</p>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-12">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fas fa-share-alt"></i></i> Share This</div>
                        <div class="card-body text-center">
                            <div class="addthis_inline_share_toolbox"></div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../../inc/footer.phtml');
        ?>
        <?php
        include('../../inc/foot.phtml');
        ?>
        <script src="<?=$config['host'];?>/assets/js/siel.js"></script>
    </body>
</html>