<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Tools';
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    $on     = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE status='ON'"));
                    $off    = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE status='OFF'"));
                    ?>
                    <div class="alert alert-success"><b><?=$on;?></b> active tools available</div>
                </div>
            </div>

            <div class="row">
                <?php
                $i=0;
                $tampil = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE status='ON' ORDER BY name ASC");
                while($data = mysqli_fetch_array($tampil, MYSQLI_ASSOC)){
                $i++;
                ?>
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-body">
                            <h4 class="card-title"><?=$data['name']?></h4>
                            <p class="card-text"><?=$data['description']?></p>
                            <a href="<?=$data['page_name']?>"><button class="btn btn-primary"><i class="fas fa-external-link-square-alt"></i> Visit Tools</button></a>
                            <?php if($data['type'] == 2){ ?>
                            <button class="btn btn-info"><i class="fa fa-tags"></i> Iklan</button>
                            <?php } ?>
                        </div>
                    </div>
                    <br>
                </div>
                <?php } ?>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script src="<?=$config['host'];?>/assets/js/siel.js"></script>
    </body>
</html>