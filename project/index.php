<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Project';
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <img class="card-img-top" src="https://res.cloudinary.com/ddw14mjcm/image/upload/v1600147890/20200915_123044_isu81p.jpg" alt="Twitter Auto Base">
                        <div class="card-body">
                            <h4 class="card-title">Twitter Auto Base</h4>
                            <p class="card-text">Twitter Auto Base it’s a console based script created for bot that works like others auto base account.</p>
                            <a href="https://github.com/nthanfp/TwitterAutoBase" class="btn btn-primary"><i class="fas fa-external-link-square-alt"></i> Visit Project</a>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <img class="card-img-top" src="https://res.cloudinary.com/ddw14mjcm/image/upload/v1600147889/likegram_fvjomz.png" alt="Like Gram">
                        <div class="card-body">
                            <h4 class="card-title">Like Gram</h4>
                            <p class="card-text">Like Gram is an Auto Instagram followers website to increase your Instagram Indonesian Followers and Likes for free.</p>
                            <a href="https://github.com/nthanfp/TwitterAutoBase" class="btn btn-primary"><i class="fas fa-external-link-square-alt"></i> Visit Project</a>
                            <a href="<?=$config['host'];?>/page/contact" class="btn btn-success float-right"><i class="fa fa-shopping-cart"></i> Buy This</a>
                        </div>
                    </div><br>
                </div>
                <div class="col-lg-6">
                    <div class="card border-primary">
                        <img class="card-img-top" src="https://res.cloudinary.com/ddw14mjcm/image/upload/v1600147890/igprice_aedadv.gif" alt="Insta Price">
                        <div class="card-body">
                            <h4 class="card-title">Insta Price</h4>
                            <p class="card-text">Insta Price allows you to calculate your estimated price your Instagram account based on your engagement and number of followers.</p>
                            <a href="https://igprice.me" class="btn btn-primary"><i class="fas fa-external-link-square-alt"></i> Visit Project</a>
                            <a href="<?=$config['host'];?>/page/contact" class="btn btn-success float-right"><i class="fa fa-shopping-cart"></i> Buy This</a>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script src="<?=$config['host'];?>/assets/js/siel.js"></script>
    </body>
</html>