<?php
session_start();
require('./lib/config.php');
$config['pagename'] = 'Main Page';
?>
<!doctype html>
<html lang="en">
    <head>
        <title><?=$config['pagename'];?> - <?=$config['name'];?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta http-equiv="Cache-Control" content="public" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="googlebot" content="index,follow"/>
        <meta name="robots" content="index,follow"/>
        <meta name="description" content="<?=$config['description'];?>">
        <meta name="keywords" content="<?=$config['keyword'];?>" />
        <meta name="author" content="<?=$config['author'];?>">
        <link rel="icon" href="<?=$config['favicon'];?>">

        <!-- Theme Color URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
        <meta name="theme-color" content="#593196"/>
        <!-- URL Theme Color untuk Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#593196" />
        <!-- URL Theme Color untuk iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#593196" />
        <!-- End Theme Color -->

        <!-- Google / Search Engine Tags -->
        <meta itemprop="name" content="<?=$config['name'];?> - <?=$config['pagename'];?>">
        <meta itemprop="description" content="<?=$config['description'];?>">
        <meta itemprop="image" content="<?=$config['og_image'];?>">
        
        <!-- Facebook Meta Tags -->
        <meta property="og:url" content="<?=$config['host'];?>">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?=$config['name'];?> - <?=$config['pagename'];?>">
        <meta property="og:description" content="<?=$config['description'];?>">
        <meta property="og:image" content="<?=$config['og_image'];?>">
        
        <!-- Twitter Meta Tags -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="<?=$config['name'];?> - <?=$config['pagename'];?>">
        <meta name="twitter:description" content="<?=$config['description'];?>">
        <meta name="twitter:image" content="<?=$config['og_image'];?>">
        <!-- Meta Tags Generated via http://heymeta.com -->

        <!-- Bootstrap core CSS -->
        <link href="<?=$config['host'];?>/assets/css/bootstrap-pulse.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?=$config['host'];?>/assets/css/custom.css" rel="stylesheet">
        <link href="<?=$config['host'];?>/assets/css/custom-landing.css" rel="stylesheet">
        <!-- Fontawesome CSS -->
        <link href="<?=$config['host_admin'];?>/plugins/fontawesome-free/css/all.min.css" rel="stylesheet">
        <!-- Datatable CSS -->
        <link href="<?=$config['host'];?>/assets/css/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Noto Sans' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css2?family=Viga" rel="stylesheet">
        <!-- Bootstrap Toggle -->
        <link href="<?=$config['host'];?>/assets/css/bootstrap4-toggle.min.css" rel="stylesheet">
        <!-- Pace CSS -->
        <link href="<?=$config['host'];?>/assets/css/pace.min.css" rel="stylesheet">
        <!-- Sweetalert CSS -->
        <link rel="stylesheet" type="text/css" href="<?=$config['host'];?>/assets/css/sweetalert.css">
        <!-- Tagsinput CSS -->
        <link rel="stylesheet" type="text/css" href="<?=$config['host'];?>/assets/css/tagsinput.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125402297-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-125402297-3');
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="#"><?=$config['name'];?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ml-auto">
                        <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="#">Pricing</a>
                        <a class="nav-item nav-link" href="#">Features</a>
                        <a class="nav-item nav-link" href="#">About</a>
                        <a class="nav-item tombol btn btn-primary" href="#">Join Us</a>
                    </div>
                </div>
            </div>
        </nav>

        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Get work done <span>faster</span><br> and <span>better</span> with us</h1>
                <a href="" class="btn btn-primary tombol">Our Work</a>
            </div>
        </div>
        
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 info-panel">
                    <div class="row">
                        <div class="col-lg">
                            <img src="assets/img/employee.png" alt="Employee" class="float-left">
                            <h4>24 Hours</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div class="col-lg">
                            <img src="assets/img/hires.png" alt="High Res" class="float-left">
                            <h4>High-Res</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div class="col-lg">
                            <img src="assets/img/security.png" alt="Security" class="float-left">
                            <h4>Security</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.container -->
        <?php
        include('./inc/footer.phtml');
        ?>
        <?php
        include('./inc/foot.phtml');
        ?>
    </body>
</html>