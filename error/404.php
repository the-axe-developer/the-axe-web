<?php
session_start();
require('../lib/config.php');
$config['pagename'] = '404 Error';
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-12">
                    <div class="error-template">
                        <h1>Oops!</h1>
                        <h2>404 Not Found</h2>
                        <div class="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                        <div class="error-actions">
                            <a href="<?=$config['host'];?>" class="btn btn-primary btn-lg"><span class="fa fa-home"></span>
                                Take Me Home </a>
                            <a href="<?=$config['host'];?>/page/contact" class="btn btn-info btn-lg"><span class="fa fa-envelope"></span> Contact Support </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script src="<?=$config['host'];?>/assets/js/siel.js"></script>
    </body>
</html>