<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Contact';
?>
<!doctype html>
<html lang="en">
    <?php
    include('../inc/head.phtml');
    ?>
    <body class="body-bg">
        <?php
        include('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white">
                            <i class="fa fa-phone"></i> Contact Us
                        </div>
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg">
                                    <i class="fa fa-envelope fa-5x" style="color:#593196;"></i><br>
                                    <span><a href="mailto:ntnferry1505@gmail.com">ntnferry1505@gmail.com</a></span>
                                </div>
                                <div class="col-lg">
                                    <i class="fab fa-instagram fa-5x" style="color:#593196;"></i><br>
                                    <span><a href="https://instagram.com/nthanfp">@nthanfp</a></span>
                                </div>
                                <div class="col-lg">
                                    <i class="fab fa-facebook fa-5x" style="color:#593196;"></i><br>
                                    <span><a href="https://facebook.com/ntanfp">Click here</a></span>
                                </div>
                            </div>
                        </div>
                    </div><br>
                </div>
                <div class="col-md-6">
                    <div class="card border-primary">
                        <div class="card-header bg-primary text-white"><i class="fa fa-envelope"></i> Contact Form</div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope text-primary"></i></div>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-phone text-primary"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-list"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-comment text-primary"></i></div>
                                    </div>
                                    <textarea class="form-control" id="message" name="message" placeholder="Message" required></textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="submit" value="Send Message" class="btn btn-primary btn-block rounded-0 py-2">
                            </div>
                        </div>
                    </div><br>
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
    </body>
</html>