<?php
session_start();
require('./lib/config.php');
$config['pagename'] = 'Main Page';
?>
<!-- HTML -->
<!doctype html>
<html lang="en">
<?php
include('./inc/head.phtml');
?>

<body class="body-img text-white">
    <?php
    include('./inc/header.phtml');
    ?>
    <main role="main" class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="siel">
                    <h1 class="lol" data-target-resolver></h1>
                </div>
            </div>
        </div>
    </main>
    <!-- /.container -->
    <?php
    include('./inc/footer.phtml');
    ?>
    <?php
    include('./inc/foot.phtml');
    ?>
    <script src="<?= $config['host']; ?>/assets/js/siel.js"></script>
    <script type="text/javascript">
        window.addEventListener('beforeinstallprompt', function(e) {
            e.userChoice.then(function(choiceResult) {
                console.log(choiceResult.outcome);
                if (choiceResult.outcome == 'dismissed') {
                    console.log('User cancelled home screen install');
                } else {
                    console.log('User added to home screen');
                }
            });
        });
    </script>
</body>

</html>