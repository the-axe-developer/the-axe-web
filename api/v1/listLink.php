<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$array = array();
$id_user    = $_SESSION['user_id'];
$id_website = $_GET['id_website'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `id_user`='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);
if ($_SESSION['user_status'] != 'login') {
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_link';
    $primaryKey     = 'id_link';
    $extraCondition = "id_link_web='" . $id_website . "'";
    $columns        = array(
        array(
            'db' => 'id_link',
            'dt' => 0,
            'field' => 'id_link'
        ),
        array(
            'db' => 'title',
            'dt' => 1,
            'field' => 'title'
        ),
        array(
            'db' => 'url',
            'dt' => 2,
            'field' => 'url'
        ),
        array(
            'db' => 'created_at',
            'dt' => 3,
            'field' => 'created_at',
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        )
    );

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, null, $extraCondition)
    );
}
