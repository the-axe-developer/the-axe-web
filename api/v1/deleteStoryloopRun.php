<?php
session_start();
require('../../lib/config.php');
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
} else {
    if($_POST['id_data']){
        $id_data    = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $delete     = mysqli_query($conn, "DELETE FROM `tbl_storyloop_run` WHERE id_sl_run='".$id_data."'");
        if($delete){
        	$array['result']    = 1;
    		$array['content']   = 'Data deleted';
        } else {
        	$array['result']    = 0;
    		$array['content']   = 'Failed '.mysqli_error($conn);
        }
    }
}
print_r(json_encode($array));