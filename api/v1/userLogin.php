<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if(empty($_POST['email'])||empty($_POST['password'])){
    $array['result']    = 0;
    $array['content']   = 'Form is required';    
} else {
    $ip         = ip();
    $uuid       = Uuid::uuid4();
    $email      = mysqli_real_escape_string($conn, stripslashes($_POST['email']));
    $password   = mysqli_real_escape_string($conn, stripslashes($_POST['password']));
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        $select = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE email='".$email."' AND password='".md5($password)."'");
        $rows   = mysqli_num_rows($select);
        if($rows == 1){
            $row = mysqli_fetch_assoc($select);
            //ob_start();
            $_SESSION['user_id']        = $row['id_user'];
            $_SESSION['user_email']     = $row['email'];
            $_SESSION['user_status']    = "login";
            $logkey                     = rand_str('all', '64');
            $ab                         = time() + (60 * 60 * 24 * 30);
            $jadigini                   = date('d-m-Y', $ab);
            setcookie('a45', base64_encode($row['email']), time()+(86400 * 30), '/');
            setcookie('a46', base64_encode($row['id_user']), time()+(86400 * 30), '/');
            setcookie('a47', $logkey, time()+(86400 * 30), '/');
            mysqli_query($conn, "INSERT INTO `tbl_sessionkey` (`id`, `id_user`, `email`, `logkey`, `expired`) VALUES ('".$uuid."', '".$row['id_user']."', '".$row['email']."', '".$logkey."', '".$jadigini."')");
            $update = mysqli_query($conn, "UPDATE `tbl_user` SET last_login='".time()."', ip_address='".$ip."' WHERE id_user='".$_SESSION['user_id']."'");
            if($update){
                $array['result']    = 1;
                if($_POST['redir']){
                    $array['content']   = "Login success, Redirecting....<script>window.location = '".$_POST['redir']."';</script>";
                } else {
                    $array['content']   = "Login success, Redirecting....<script>window.location = '".$config['host']."/account/dashboard';</script>";
                }
            }
        } else {
            $array['result']    = 0;
            $array['content']   = 'Email/password is incorrect';
        }
    } else {            
        $array['result']    = 0;
        $array['content']   = 'Email not valid!';
    }
}
print_r(json_encode($array));
//print_r($_COOKIE);
?>