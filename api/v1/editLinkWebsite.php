<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');

use Ramsey\Uuid\Uuid;

$array = array();
if ($_SESSION['user_status'] != 'login') {
    header('Location:' . $config['host'] . '/account/auth');
    exit();
} else {
    if ($_POST['id_data'] || $_POST['title'] || $_POST['description']) {
        $uuid           = Uuid::uuid4();
        $id_user        = $_SESSION['user_id'];
        $id_data        = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $title          = mysqli_real_escape_string($conn, stripslashes($_POST['title']));
        $description    = mysqli_real_escape_string($conn, stripslashes($_POST['description']));
        $slug           = mysqli_real_escape_string($conn, stripslashes($_POST['slug']));
        $theme          = mysqli_real_escape_string($conn, stripslashes($_POST['theme']));
        $time           = time();
        $input          = mysqli_query($conn, "UPDATE `tbl_link_web` SET `title`='$title', `description`='$description', `slug`='$slug', `theme`='$theme', `updated_at`='$time' WHERE `id_link_web`='$id_data'");
        if ($input) {
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed ' . mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
