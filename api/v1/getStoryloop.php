<?php
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');
$array = array();
if($_SESSION['user_status'] != 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['id']){
        $id_data    = mysqli_real_escape_string($conn, stripslashes($_POST['id']));
        $select     = mysqli_query($conn, "SELECT * FROM `tbl_storyloop` WHERE id_storyloop='".$id_data."'");
        $data       = mysqli_fetch_assoc($select);
        $target     = json_decode($data['target'], true);
        $answer     = json_decode($data['answer'], true);
        if(is_array($target)){
            $data['target'] = '';
            for($i=0; $i < count($target); $i++){ 
                $data['target'] .= $target[$i].","; 
            }
        }
        if(is_array($answer)){
            $data['answer'] = '';
            for($i=0; $i < count($answer); $i++){ 
                $data['answer'] .= $answer[$i].","; 
            }
        }
        print_r(json_encode($data));
    }
}