<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_tools';
    $primaryKey     = 'id';
    $columns        = array(
        array('db' => 'id', 'dt' => 0),
        array('db' => 'name','dt' => 1),
        array('db' => 'description', 'dt' => 2),
        array('db' => 'page_name', 'dt' => 3),
        array('db' => 'status', 'dt' => 4),
    );
     
    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}