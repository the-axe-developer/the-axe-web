<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['id']){
        $idtools    = mysqli_real_escape_string($conn, stripslashes($_POST['id']));
        $select     = mysqli_query($conn, "SELECT * FROM `tbl_tools` WHERE id='".$idtools."'");
        print_r(json_encode(mysqli_fetch_assoc($select)));
    }
}