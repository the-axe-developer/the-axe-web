<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['parameter']||$_POST['required']||$_POST['description']){
        $id_data    = mysqli_real_escape_string($conn, stripcslashes($_POST['id_data']));
        $id_api     = mysqli_real_escape_string($conn, stripcslashes($_POST['id_api']));
        $parameter  = mysqli_real_escape_string($conn, stripslashes($_POST['parameter']));
        $required   = mysqli_real_escape_string($conn, stripslashes($_POST['required']));
        $desc       = mysqli_real_escape_string($conn, stripslashes($_POST['description']));
        $update     = mysqli_query($conn, "UPDATE `tbl_api_params` SET id_api='$id_api', parameter='$parameter', required='$required', description='$desc' WHERE id_param='$id_data'");
        if($update){
            $array['result']    = 1;
            $array['content']   = 'Data Updated';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed: '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>