<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if(!empty($_POST['password'])||!empty($_POST['confirm_password'])){
        $id                 = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $password           = mysqli_real_escape_string($conn, stripslashes($_POST['password']));
        $confirm_password   = mysqli_real_escape_string($conn, stripslashes($_POST['confirm_password']));
        $uppercase          = preg_match('@[A-Z]@', $password);
        $lowercase          = preg_match('@[a-z]@', $password);
        $number             = preg_match('@[0-9]@', $password);
        if(!$uppercase||!$lowercase||!$number||strlen($password)< 8){
            $array['result']    = 0;
            $array['content']   = 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
        } else {
            if($password == $confirm_password){
                $update = mysqli_query($conn, "UPDATE `tbl_user` SET password = '".md5($password)."' WHERE id_user='".$id."'");
                if($update){
                    $array['result']    = 1;
                    $array['content']   = 'Success update data to database';
                } else {
                    $array['result']    = 0;
                    $array['content']   = 'Error '.mysqli_error($conn);
                }
            } else {
                $array['result']    = 0;
                $array['content']   = 'Password and confirm password not match.';
            }
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What do you do?';
    }
}
print_r(json_encode($array));