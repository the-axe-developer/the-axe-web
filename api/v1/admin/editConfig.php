<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['host']){
        $host    	= mysqli_real_escape_string($conn, stripslashes($_POST['host']));
        $name    	= mysqli_real_escape_string($conn, stripslashes($_POST['name']));
        $title   	= mysqli_real_escape_string($conn, stripslashes($_POST['title']));
        $desc    	= mysqli_real_escape_string($conn, stripslashes($_POST['desc']));
        $keyword 	= mysqli_real_escape_string($conn, stripslashes($_POST['keyword']));
        $author  	= mysqli_real_escape_string($conn, stripslashes($_POST['author']));
        $ogimage 	= mysqli_real_escape_string($conn, stripslashes($_POST['ogimg']));
        $favicon 	= mysqli_real_escape_string($conn, stripslashes($_POST['favicon']));
        $update 	= mysqli_query($conn, "UPDATE tbl_config SET host='".$host."', name='".$name."', title='".$title."', description='".$desc."', keyword='".$keyword."', author='".$author."', og_image='".$ogimage."', favicon='".$favicon."' WHERE id='1'");
        if($update){
            $array['result']    = 1;
			$array['content']   = 'Success update data';
        } else {
            $array['result']    = 0;
			$array['content']   = 'Error '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>