<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
require('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['parameter']||$_POST['required']||$_POST['description']){
        $uuid       = Uuid::uuid4();
        $id_api     = mysqli_real_escape_string($conn, stripcslashes($_POST['id_api']));
        $parameter  = mysqli_real_escape_string($conn, stripslashes($_POST['parameter']));
        $required   = mysqli_real_escape_string($conn, stripslashes($_POST['required']));
        $desc       = mysqli_real_escape_string($conn, stripslashes($_POST['description']));
        $input      = mysqli_query($conn, "INSERT INTO `tbl_api_params` (`id_param`, `id_api`, `parameter`, `required`, `description`) VALUES ('$uuid', '$id_api', '$parameter', '$required', '$desc')");
        if($input){
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>