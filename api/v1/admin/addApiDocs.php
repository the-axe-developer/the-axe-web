<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
require('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['title']||$_POST['method']||$_POST['endpoint']||$_POST['result']){
        $uuid       = Uuid::uuid4();
        $title      = mysqli_real_escape_string($conn, stripslashes($_POST['title']));
        $action     = mysqli_real_escape_string($conn, stripslashes($_POST['method']));
        $site       = mysqli_real_escape_string($conn, stripslashes($_POST['endpoint']));
        $result     = mysqli_real_escape_string($conn, stripslashes($_POST['result']));
        $input      = mysqli_query($conn, "INSERT INTO `tbl_api_docs` (`id_api`, `title`, `action`, `site`, `result`) VALUES ('$uuid', '$title', '$action', '$site', '$result')");
        if($input){
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>