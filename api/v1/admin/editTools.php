<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['name']||$_POST['site']||$_POST['ket']){
        $idtools    = mysqli_real_escape_string($conn, stripcslashes($_POST['id_tools']));
        $nama       = mysqli_real_escape_string($conn, stripslashes($_POST['name']));
        $site       = mysqli_real_escape_string($conn, stripslashes($_POST['site']));
        $keterangan = mysqli_real_escape_string($conn, stripslashes($_POST['ket']));
        $stat       = mysqli_real_escape_string($conn, stripslashes($_POST['status']));
        $update     = mysqli_query($conn, "UPDATE `tbl_tools` SET name='".$nama."', description='".$keterangan."', page_name='".$site."', status='".$stat."' WHERE id='".$idtools."'");
        if($update){
            $array['result']    = 1;
            $array['content']   = 'Data Updated';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed: '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>