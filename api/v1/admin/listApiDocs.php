<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_api_docs';
    $primaryKey     = 'id_api';
    $columns        = array(
        array('db' => 'id_api', 'dt' => 0),
        array('db' => 'title','dt' => 1),
        array(
            'db' => 'action',
            'dt' => 2,
            'formatter' => function( $d, $row ){
                return '<span class="badge badge-primary">'.$d.'</span>';
            }
        ),
        array('db' => 'site', 'dt' => 3, )
    );
     
    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}