<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_api_history';
    $primaryKey     = 'id_history';
    $joinQuery      = "FROM `{$table}` AS `re` INNER JOIN `tbl_user` AS `us` ON (`re`.`id_user` = `us`.`id_user`)";
    //$extraCondition = "`id_client`=".$ID_CLIENT_VALUE;
    //$where          = 'tbl_instagram_log.id_instagram=tbl_instagram.id_instagram';
    $columns        = array(
        array(
            'db' => '`re`.`created_at`',
            'dt' => 0,
            'field' => 'created_at',
            'formatter' => function($d, $row){
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => '`us`.`email`',
            'dt' => 1,
            'field' => 'email',
            'formatter' => function($d, $row){
                return $d;
            }
        ),
        array(
            'db' => '`re`.`name`',
            'dt' => 2,
            'field' => 'name',
            'formatter' => function($d, $row){
                return $d;
            }
        ),
        array(
            'db' => '`re`.`ip_address`',
            'dt' => 3,
            'field' => 'ip_address',
            'formatter' => function($d, $row){
                return $d;
            }
        ),
        array(
            'db' => '`re`.`data`',
            'dt' => 4,
            'field' => 'data',
            'formatter' => function($d, $row){
                return $d;
            }
        )
    );
     
    echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraCondition));
}