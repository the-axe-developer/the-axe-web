<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['id_data']){
        $id_data    = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $delete     = mysqli_query($conn, "DELETE FROM `tbl_api_docs` WHERE id_api='".$id_data."'");
        if($delete){
        	$array['result']    = 1;
    		$array['content']   = 'Data deleted';
        } else {
        	$array['result']    = 0;
    		$array['content']   = 'Failed '.mysqli_error($conn);
        }
    }
}
print_r(json_encode($array));