<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_instagram';
    $primaryKey     = 'id';
    $columns        = array(
        array('db' => 'id', 'dt' => 0),
        array('db' => 'user_id','dt' => 1),
        array('db' => 'username', 'dt' => 2),
        array('db' => 'password', 'dt' => 3),
        array(
            'db' => 'status',
            'dt' => 4,
            'formatter' => function($d, $row){
                if($d == 'yes'){
                    $x = '<span class="badge badge-success">Yes</span>';
                } elseif($d == 'no'){
                    $x = '<span class="badge badge-danger">No</span>';
                }
                return $x;
            }
        )
    );
     
    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}