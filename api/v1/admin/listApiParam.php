<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_api_params';
    $where          = 'tbl_api_params.id_api=tbl_api_docs.id_api';
    $primaryKey     = 'id_param';
    $columns        = array(
        array(
            'db' => 'id_param',
            'dt' => 0
        ),
        array('db' => 'id_api',
            'dt' => 1,
            'formatter' => function($d, $row){
                global $conn;
                $xyz = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM tbl_api_docs WHERE id_api='$d'"));
                return $xyz['title'];
            }
        ),
        array('db' => 'parameter', 'dt' => 2),
        array(
            'db' => 'required',
            'dt' => 3,
            'formatter' => function($d, $row){
                return '<span class="badge badge-primary">'.strtoupper($d).'</span>';
            }
        ),
        array('db' => 'description', 'dt' => 4)
    );
     
    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}