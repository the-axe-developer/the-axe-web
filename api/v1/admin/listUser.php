<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if ($_SESSION['admin_status'] != 'login') {
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_user';
    //$where          = 'tbl_api_params.id_api=tbl_api_docs.id_api';
    $primaryKey     = 'id_user';
    $columns        = array(
        array(
            'db' => 'id_user',
            'dt' => 0
        ),
        array(
            'db' => 'email',
            'dt' => 1
        ),
        array(
            'db' => 'first_name',
            'dt' => 2
        ),
        array(
            'db' => 'last_name',
            'dt' => 3
        ),
        array(
            'db' => 'apikey',
            'dt' => 4,
            'formatter' => function ($d, $row) {
                return '<input type="text" class="form-control" value="' . $d . '" disabled="true">';
            }
        ),
        array(
            'db' => 'registered_date',
            'dt' => 5,
            'formatter' => function ($d, $row) {
                return date('Y-m-d H:i:s', $d);
            }
        ),
        array(
            'db' => 'last_login',
            'dt' => 6,
            'formatter' => function ($d, $row) {
                $time = (!$d) ? 0 : $d;

                return date('Y-m-d H:i:s', $time);
            }
        ),
        array(
            'db' => 'ip_address',
            'dt' => 7
        )
    );

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}
