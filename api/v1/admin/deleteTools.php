<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['id']){
        $idtools    = mysqli_real_escape_string($conn, stripslashes($_POST['id']));
        $delete     = mysqli_query($conn, "DELETE FROM `tbl_tools` WHERE id='".$idtools."'");
        if($delete){
        	$array['result']    = 1;
    		$array['content']   = 'Data deleted';
        } else {
        	$array['result']    = 0;
    		$array['content']   = 'Failed '.mysqli_error($conn);
        }
    }
}
print_r(json_encode($array));