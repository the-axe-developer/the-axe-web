<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['name']||$_POST['site']||$_POST['ket']){
        $nama       = mysqli_real_escape_string($conn, stripslashes($_POST['name']));
        $site       = mysqli_real_escape_string($conn, stripslashes($_POST['site']));
        $keterangan = mysqli_real_escape_string($conn, stripslashes($_POST['ket']));
        $type       = mysqli_real_escape_string($conn, stripslashes($_POST['type']));
        $stat       = mysqli_real_escape_string($conn, stripslashes($_POST['status']));
        $input      = mysqli_query($conn, "INSERT INTO `tbl_tools` (name, description, page_name, status) VALUES ('".$nama."', '".$keterangan."','".$site."', '".$stat."')");
        if($input){
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>