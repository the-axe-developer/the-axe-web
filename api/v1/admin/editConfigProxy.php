<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $proxy_ip       = mysqli_real_escape_string($conn, stripslashes($_POST['proxy_ip']));
    $proxy_auth     = mysqli_real_escape_string($conn, stripslashes($_POST['proxy_auth']));
    $proxy_socks5   = mysqli_real_escape_string($conn, stripslashes($_POST['proxy_socks5']));
    $update         = mysqli_query($conn, "UPDATE tbl_config SET proxy_ip='".$proxy_ip."', proxy_auth='".$proxy_auth."', proxy_socks5='".$proxy_socks5."' WHERE id='1'");
    if($update){
        $array['result']    = 1;
        $array['content']   = 'Success update proxy data';
    } else {
        $array['result']    = 0;
        $array['content']   = 'Error '.mysqli_error($conn);
    }
}
print_r(json_encode($array));
?>