<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['title']||$_POST['method']||$_POST['endpoint']||$_POST['result']){
        $id_data    = mysqli_real_escape_string($conn, stripcslashes($_POST['id_data']));
        $title      = mysqli_real_escape_string($conn, stripslashes($_POST['title']));
        $action     = mysqli_real_escape_string($conn, stripslashes($_POST['method']));
        $site       = mysqli_real_escape_string($conn, stripslashes($_POST['endpoint']));
        $result     = mysqli_real_escape_string($conn, stripslashes($_POST['result']));
        $update     = mysqli_query($conn, "UPDATE `tbl_api_docs` SET title='$title', action='$action', site='$site', result='$result' WHERE id_api='$id_data'");
        if($update){
            $array['result']    = 1;
            $array['content']   = 'Data Updated';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed: '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>