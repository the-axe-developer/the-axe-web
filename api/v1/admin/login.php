<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if(empty($_POST['email'])||empty($_POST['password'])){
    $array['result']    = 0;
    $array['content']   = 'Form is required';    
} else {
    $email      = mysqli_real_escape_string($conn, stripslashes($_POST['email']));
    $password   = mysqli_real_escape_string($conn, stripslashes($_POST['password']));
    $captcha    = mysqli_real_escape_string($conn, stripslashes($_POST['g-recaptcha-response']));
    if(!$captcha){
        $array['result']    = 0;
        $array['content']   = 'Please check the the captcha form.';
    } else {
        $secretKey      = $config['recaptcha_secret_key'];
        $ip             = $_SERVER['REMOTE_ADDR'];
        $url            = 'https://www.google.com/recaptcha/api/siteverify';
        $data           = 'secret='.$secretKey.'&response='.$captcha.'&remoteip='.ip();
        $response       = basic_cURL($url, 0, 0, $data, array('Content-type: application/x-www-form-urlencoded'));
        $responseKeys   = json_decode($response[1], true);
        if($responseKeys['success']){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $select = mysqli_query($conn, "SELECT * FROM `tbl_admin` WHERE email='".$email."' AND password='".md5($password)."'");
                $rows   = mysqli_num_rows($select);
                if($rows == 1){
                    while($row = mysqli_fetch_assoc($select)){
                        $_SESSION['admin_id']     = $row['id'];
                        $_SESSION['admin_email']  = $row['email'];
                        $_SESSION['admin_status'] = "login";
                    }
                    $update = mysqli_query($conn, "UPDATE `tbl_admin` SET last_login='".date('d-m-Y H:i:s')."' WHERE id='".$_SESSION['id']."'");
                    if($update){
                        $array['result']    = 1;
                        $array['content']   = "Login success, Redirecting....<script>window.location = '".$config['host']."/admin-page';</script>";
                    }
                } else {
                    $array['result']    = 0;
                    $array['content']   = 'Email/password is incorrect';
                }
            } else {            
                $array['result']    = 0;
                $array['content']   = 'Email not valid!';
            }
        } else {
            $array['result']    = 0;
            $array['content']   = 'Captcha not valid, Please refresh this page';
        }
    }
}
print_r(json_encode($array));
?>