<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
	$array['content']   = 'Session error';
} else {
    if($_POST['site_key']||$_POST['secret_key']){
        $site_key    	= mysqli_real_escape_string($conn, stripslashes($_POST['site_key']));
        $secret_key    	= mysqli_real_escape_string($conn, stripslashes($_POST['secret_key']));
        $update         = mysqli_query($conn, "UPDATE tbl_config SET recaptcha_site_key='".$site_key."', recaptcha_secret_key='".$secret_key."' WHERE id='1'");
        if($update){
            $array['result']    = 1;
			$array['content']   = 'Success update data';
        } else {
            $array['result']    = 0;
			$array['content']   = 'Error '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>