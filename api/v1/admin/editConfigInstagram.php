<?php
session_start();
header('Content-Type: application/json');
require('../../../lib/config.php');
$array = array();
if($_SESSION['admin_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['ig_cookie']||$_POST['ig_useragent']){
        $cookie         = mysqli_real_escape_string($conn, stripslashes($_POST['ig_cookie']));
        $useragent      = mysqli_real_escape_string($conn, stripslashes($_POST['ig_useragent']));
        $update         = mysqli_query($conn, "UPDATE tbl_config SET ig_cookie_default='".$cookie."', ig_useragent_default='".$useragent."' WHERE id='1'");
        if($update){
            $array['result']    = 1;
            $array['content']   = 'Success update instagram data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Error '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>