<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
} else {
    if($_POST['id_instagram']||$_POST['id_storyloop']){
        $uuid           = Uuid::uuid4();
        $id_user        = $_SESSION['user_id'];
        $id_ig          = mysqli_real_escape_string($conn, stripslashes($_POST['id_instagram']));
        $id_sl          = mysqli_real_escape_string($conn, stripslashes($_POST['id_storyloop']));
        $time           = time();
        $input          = mysqli_query($conn, "INSERT INTO `tbl_storyloop_run` (`id_sl_run`, `id_user`, `id_instagram`, `id_storyloop`, `id_task`, `created_at`) VALUES ('$uuid', '$id_user', '$id_ig', '$id_sl', '', '$time')");
        if($input){
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>