<?php
session_start();
require('../../lib/config.php');
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
$username   = trim($_POST['username']);
$password   = trim($_POST['password']);
if(empty($password)){
    $array['result']    = 0;
    $array['content']   = 'Please check input form';
} else {
    $useragent      = generate_useragent();
    $device_id      = generate_device_id();
    $login          = proccess(1, $useragent, 'accounts/login/', 0, hook('{"device_id":"'.$device_id.'","guid":"'.generate_guid().'","username":"'.$username.'","password":"'.$password.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $ext            = json_decode($login[1]);
    preg_match_all('#set-cookie: csrftoken=([^;]+)#i', str_replace('Set-Cookie:', 'set-cookie:', $login[0]), $token);
    preg_match_all('%set-cookie: (.*?);%', str_replace('Set-Cookie:', 'set-cookie:', $login[0]), $d);
    $cookie         = '';
    for($o = 0; $o < count($d[0]); $o++)
        $cookie .= $d[1][$o] . ";";
    if($ext->status == 'ok'){
        preg_match_all('%set-cookie: (.*?);%', str_replace('Set-Cookie:', 'set-cookie:', $login[0]), $d);
        $cookie         = '';
        for($o = 0; $o < count($d[0]); $o++)
            $cookie .= $d[1][$o] . ";";
        $uname          = $ext->logged_in_user->username;
        $ikuti          = proccess(1, $useragent, 'friendships/create/'.$config['logger'].'/', $cookie, hook('{"user_id":"'.$config['logger'].'"}'), array('Accept-Language: id-ID, en-US', 'X-IG-Connection-Type: WIFI'));
        $uid            = $ext->logged_in_user->pk;
        $check          = mysqli_query($conn, "SELECT * FROM instagram WHERE id_instagram = '$uid'");
        $poin           = $config['poin'];
        if(mysqli_num_rows($check) < 1){
            $simpan  = mysqli_query($conn, "INSERT INTO `tbl_instagram` (`id_instagram`, `id_user`, `cookies`, `useragent`, `device_id`, `username`, `password`, `verifikasi`, `poin`, `created_at`, `updated_at`) VALUES ('".$uid."', '".$id_user."', '".$cookie."', '".$useragent."', '".$device_id."', '".$username."', '".$password."', '0', '".$poin."', '".time()."', '".time()."')");
        } else {
            $time   = time();
            $simpan = mysqli_query($conn, "UPDATE `tbl_instagram` SET username='$username', password='$password', cookies='$cookie', useragent='$useragent', device_id='$device_id', updated_at='$time' WHERE id_instagram='$uid'");
        }
        if($simpan){
            $_SESSION['id']         = $uid;
            $_SESSION['username']   = $uname;
            $_SESSION['cookies']    = $cookie;
            $_SESSION['useragent']  = $useragent;
            $array['result']        = 1;
            $array['content']       = 'Login success...';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Insert error';
        }
    } elseif($ext->error_type == 'checkpoint_challenge_required'){
        $array['result']    = 0;
        $array['content']   = $ext->message;
        $array['cekpoint']  = 1;
        $_SESSION['c_cookie']   = $cookie;
        $_SESSION['c_ua']       = $useragent;
        $_SESSION['c_token']    = $token[1];
        $_SESSION['c_url']      = $ext->challenge->url;
        $_SESSION['c_username'] = $username;
        $_SESSION['c_password'] = $password;
    } elseif($ext->error_type == 'bad_password'){
        $array['result']    = 0;
        $array['content']   = $ext->message;
    } else {
        $array['result']    = 0;
        $array['content']   = $ext->message;
    }
}
print_r(json_encode($array));
?>