<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if(empty($_POST['email'])||empty($_POST['password'])||empty($_POST['f_name'])||empty($_POST['l_name'])){
    $array['result']    = 0;
    $array['content']   = 'Form is required';
} else {
    $uuid       = Uuid::uuid4();
    $email      = mysqli_real_escape_string($conn, stripcslashes($_POST['email']));
    $password   = mysqli_real_escape_string($conn, stripslashes($_POST['password']));
    $c_password = mysqli_real_escape_string($conn, stripslashes($_POST['c_password']));
    $f_name     = mysqli_real_escape_string($conn, stripslashes(ucwords($_POST['f_name'])));
    $l_name     = mysqli_real_escape_string($conn, stripslashes(ucwords($_POST['l_name'])));
    $captcha    = mysqli_real_escape_string($conn, stripslashes($_POST['g-recaptcha-response']));
    if(!$captcha){
        $array['result']    = 0;
        $array['content']   = 'Please check the the captcha form, Reload this page';
    } else {
        $secretKey      = $config['recaptcha_secret_key'];
        $ip             = ip();
        $url            = 'https://www.google.com/recaptcha/api/siteverify';
        $data           = 'secret='.$secretKey.'&response='.$captcha.'&remoteip='.ip();
        $response       = basic_cURL($url, 0, 0, $data, array('Content-type: application/x-www-form-urlencoded'));
        $responseKeys   = json_decode($response[1], true);
        if($responseKeys['success']){
            $check      = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tbl_user WHERE email = '".$email."'"));
            if($check > 0){
                $array['result']    = 0;
                $array['content']   = 'Email already registered';
            } else {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $uppercase      = preg_match('@[A-Z]@', $password);
                    $lowercase      = preg_match('@[a-z]@', $password);
                    $number         = preg_match('@[0-9]@', $password);
                    if(strlen($password)< 6){
                        $array['result']    = 0;
                        $array['content']   = 'Password should be at least 6 characters.';
                    } else {
                        if($password == $c_password){
                            $insert = mysqli_query($conn, "INSERT INTO `tbl_user` (`id_user`, `email`, `password`, `first_name`, `last_name`, `registered_date`, `ip_address`) VALUES ('".$uuid."', '".$email."', '".md5($password)."', '".$f_name."', '".$l_name."', '".time()."', '".$ip."')");
                            if($insert){
                                $array['result']    = 1;
                                $array['content']   = "Register success, please login to continue";
                            } else {
                                $array['result']    = 0;
                                $array['content']   = 'Uhoh, something has gone wrong! '.mysqli_error($conn);
                            }
                        } else {
                            $array['result']    = 0;
                            $array['content']   = 'Password and confirm password not match!';
                        }
                    }                    
                } else {            
                    $array['result']    = 0;
                    $array['content']   = 'Email not valid!';
                }
            }
        } else {
            $array['result']    = 0;
            $array['content']   = 'Captcha not valid, Please reaload this page';
        }
    }
}
print_r(json_encode($array));
//print_r($_COOKIE);
?>