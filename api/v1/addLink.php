<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');

use Ramsey\Uuid\Uuid;

$array = array();
if ($_SESSION['user_status'] != 'login') {
    header('Location:' . $config['host'] . '/account/auth');
    exit();
} else {
    if ($_POST['id_website'] || $_POST['title'] || $_POST['url']) {
        $uuid           = Uuid::uuid4();
        $id_user        = $_SESSION['user_id'];
        $id_website     = mysqli_real_escape_string($conn, stripslashes($_POST['id_website']));
        $title          = mysqli_real_escape_string($conn, stripslashes($_POST['title']));
        $url            = mysqli_real_escape_string($conn, stripslashes($_POST['url']));
        $time           = time();
        $input          = mysqli_query($conn, "INSERT INTO `tbl_link` (`id_link`, `id_link_web`, `id_user`, `url`, `title`, `created_at`, `updated_at`) VALUES ('$uuid', '$id_website', '$id_user', '$url', '$title', '$time', '$time')");
        if ($input) {
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed ' . mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
        $array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
