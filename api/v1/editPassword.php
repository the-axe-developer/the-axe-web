<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
if(empty($_POST['o_password'])||empty($_POST['n_password']||empty($_POST['c_password']))){
    $array['result']    = 0;
    $array['content']   = 'Form is required';
} else {
    $account_id         = $id_user;
    $current_password   = mysqli_real_escape_string($conn, stripslashes(md5($_POST['o_password'])));
    $new_password       = mysqli_real_escape_string($conn, stripslashes($_POST['n_password']));
    $new_password_conf  = mysqli_real_escape_string($conn, stripslashes($_POST['c_password']));
    $uppercase          = preg_match('@[A-Z]@', $new_password);
    $lowercase          = preg_match('@[a-z]@', $new_password);
    $number             = preg_match('@[0-9]@', $new_password);
    $query              = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='".$account_id."' AND password='".$current_password."'");
    if(mysqli_num_rows($query) < 1){
        $array['result']    = 0;
        $array['content']   = 'Wrong current password';
    } else {
        if(strlen($new_password)< 8){
            $array['result']    = 0;
            $array['content']   = 'Password should be at least 8 characters.';
        } else {
            if($new_password == $new_password_conf){
                $update = mysqli_query($conn, "UPDATE `tbl_user` SET password = '".md5($new_password)."' WHERE id_user='".$account_id."'");
                if($update){
                    $array['result']    = 1;
                    $array['content']   = 'Success change password, please logout and login again';
                } else {
                    $array['result']    = 0;
                    $array['content']   = 'Error to change password, please contact our customer support';
                }
            } else {
                $array['result']    = 0;
                $array['content']   = 'New password and confirm password not match';
            }
        }
    }
}
print_r(json_encode($array));
?>