<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$array = array();
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `id_user`='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);
if ($_SESSION['user_status'] != 'login') {
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_link_web';
    $primaryKey     = 'id_link_web';
    //$joinQuery      = "FROM `{$table}` AS `re` INNER JOIN `tbl_instagram` AS `us` ON (`re`.`id_instagram` = `us`.`id_instagram`)";
    $extraCondition = "id_user='" . $id_user . "'";
    //$where          = 'tbl_instagram_log.id_instagram=tbl_instagram.id_instagram';
    $columns        = array(
        array(
            'db' => 'id_link_web',
            'dt' => 0,
            'field' => 'id_link_web'
        ),
        array(
            'db' => 'title',
            'dt' => 1,
            'field' => 'title'
        ),
        array(
            'db' => 'description',
            'dt' => 2,
            'field' => 'description'
        ),
        array(
            'db' => 'created_at',
            'dt' => 3,
            'field' => 'created_at',
            'formatter' => function ($d, $row) {
                return date('Y-m-d H:i:s', $d);
            }
        )
    );

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, null, $extraCondition)
    );
}
