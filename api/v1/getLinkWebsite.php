<?php
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');
$array = array();
if ($_SESSION['user_status'] != 'login') {
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if ($_POST['id']) {
        $id_data    = mysqli_real_escape_string($conn, stripslashes($_POST['id']));
        $select     = mysqli_query($conn, "SELECT * FROM `tbl_link_web` WHERE `id_link_web`='" . $id_data . "'");
        $data       = mysqli_fetch_assoc($select);
        print_r(json_encode($data));
    }
}
