<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$array = array();
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
if($_SESSION['user_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_storyloop';
    $primaryKey     = 'id_storyloop';
    //$joinQuery      = "FROM `{$table}` AS `re` INNER JOIN `tbl_instagram` AS `us` ON (`re`.`id_instagram` = `us`.`id_instagram`)";
    $extraCondition = "id_user='".$id_user."'";
    //$where          = 'tbl_instagram_log.id_instagram=tbl_instagram.id_instagram';
    $columns        = array(
        array(
            'db' => 'id_storyloop',
            'dt' => 0,
            'field' => 'id_storyloop'
        ),
        array(
            'db' => 'name',
            'dt' => 1,
            'field' => 'name'
        ),
        array(
            'db' => 'target',
            'dt' => 2,
            'field' => 'target',
            'formatter' => function($d, $row){
                $data   = json_decode($d, true);
                $x      = '';
                for ($i=0; $i < count($data); $i++) { 
                    $x .= '<span class="badge badge-primary">'.$data[$i].'<span data-role="remove"></span></span> '; 
                }
                return $x;
            }
        ),
        array(
            'db' => 'answer',
            'dt' => 3,
            'field' => 'answer',
            'formatter' => function($d, $row){
                $data   = json_decode($d, true);
                $x      = '';
                for ($i=0; $i < count($data); $i++) { 
                    $x .= '<span class="badge badge-primary">'.$data[$i].'<span data-role="remove"></span></span> '; 
                }
                return $x;
            }
        ),
        array(
            'db' => 'target_count',
            'dt' => 4,
            'field' => 'target_count'
        ),
        array(
            'db' => 'sleep_1',
            'dt' => 5,
            'field' => 'sleep_1',
            'formatter' => function($d, $row){
                return $d.' Second';
            }
        ),
        array(
            'db' => 'sleep_2',
            'dt' => 6,
            'field' => 'sleep_2',
            'formatter' => function($d, $row){
                return $d.' Second';
            }
        ),
        array(
            'db' => 'created_at',
            'dt' => 7,
            'field' => 'created_at',
            'formatter' => function($d, $row){
                return date('Y-m-d H:i:s', $d);
            }
        )
    );
     
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, null, $extraCondition)
    );
}