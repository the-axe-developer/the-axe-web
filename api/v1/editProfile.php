<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
}
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
if(empty($_POST['f_name'])||empty($_POST['l_name'])){
    $array['result']    = 0;
    $array['content']   = 'Form is required';
} else {
    $account_id         = $id_user;
    $first_name         = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars(xss_clean($_POST['f_name']))));
    $last_name          = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars(xss_clean($_POST['l_name']))));
    $phone              = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars(xss_clean($_POST['phone']))));
    $gender             = mysqli_real_escape_string($conn, stripslashes(htmlspecialchars(xss_clean($_POST['gender']))));
    $birthday           = mysqli_real_escape_string($conn, stripslashes($_POST['birthday']));
    $query              = mysqli_query($conn, "UPDATE `tbl_user` SET first_name='$first_name', last_name='$last_name', phone='$phone', gender='$gender', birthday='$birthday' WHERE id_user='$id_user'");
    if($query){
        $array['result']    = 1;
        $array['content']   = 'Success update profile';
    } else {
        $array['result']    = 0;
        $array['content']   = 'Error database: '.mysqli_error();
    }
}
print_r(json_encode($array));
?>