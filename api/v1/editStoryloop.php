<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
require('../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$array = array();
if($_SESSION['user_status'] != 'login'){
    header('Location:'.$config['host'].'/account/auth');
    exit();
} else {
    if($_POST['name']||$_POST['target']||$_POST['answer']||$_POST['target_count']||$_POST['sleep_1']||$_POST['sleep_2']){
        $uuid           = Uuid::uuid4();
        $id_user        = $_SESSION['user_id'];
        $id_data        = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $name           = mysqli_real_escape_string($conn, stripslashes($_POST['name']));
        $target         = json_encode(preg_split("/,/", mysqli_real_escape_string($conn, stripslashes($_POST['target']))));
        $answer         = json_encode(preg_split("/,/", mysqli_real_escape_string($conn, stripslashes($_POST['answer']))));
        $target_count   = mysqli_real_escape_string($conn, stripslashes($_POST['target_count']));
        $sleep_1        = mysqli_real_escape_string($conn, stripslashes($_POST['sleep_1']));
        $sleep_2        = mysqli_real_escape_string($conn, stripslashes($_POST['sleep_2']));
        $input          = mysqli_query($conn, "UPDATE `tbl_storyloop` SET `name`='$name', `target`='$target', `answer`='$answer', `target_count`='$target_count', `sleep_1`='$sleep_1', `sleep_2`='$sleep_2' WHERE id_storyloop='$id_data'");
        if($input){
            $array['result']    = 1;
            $array['content']   = 'Success save data';
        } else {
            $array['result']    = 0;
            $array['content']   = 'Failed '.mysqli_error($conn);
        }
    } else {
        $array['result']    = 0;
		$array['content']   = 'What are u do?';
    }
}
print_r(json_encode($array));
?>