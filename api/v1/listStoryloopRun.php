<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$array = array();
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
if($_SESSION['user_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_storyloop_run';
    $primaryKey     = 'id_sl_run';
    $joinQuery      = "FROM ((`$table` AS `slr` INNER JOIN `tbl_storyloop` AS `sl` ON `slr`.`id_storyloop`=`sl`.`id_storyloop`) INNER JOIN `tbl_instagram` AS `ig` ON `slr`.`id_instagram`=`ig`.`id_instagram`)";
    $extraCondition = "`slr`.`id_user`='".$id_user."'";
    //$where          = 'tbl_instagram_log.id_instagram=tbl_instagram.id_instagram';
    $columns        = array(
        array(
            'db' => '`slr`.`id_sl_run`',
            'dt' => 0,
            'field' => 'id_sl_run'
        ),
        array(
            'db' => '`ig`.`username`',
            'dt' => 1,
            'field' => 'username'
        ),
        array(
            'db' => '`sl`.`name`',
            'dt' => 2,
            'field' => 'name'
        ),
        array(
            'db' => '`slr`.`id_task`',
            'dt' => 3,
            'field' => 'id_task',
            'formatter' => function($d, $row){
                if($d == null){
                    return '<label class="badge badge-danger">OFF</label>';
                } else if(is_numeric($d)){
                    return '<label class="badge badge-success">RUNNING</label>';
                } else {
                    return false;
                }
            }
        ),
        array(
            'db' => '`slr`.`created_at`',
            'dt' => 4,
            'field' => 'created_at',
            'formatter' => function($d, $row){
                return date('Y-m-d H:i:s', $d);
            }
        ),
        array(
            'db' => '`slr`.`id_task`',
            'dt' => 5,
            'field' => 'id_task',
            'formatter' => function($d, $row){
                if($d == null){
                    return "<button class='btn btn-success btn-sm tblRun'>Run <i class='fa fa-play'></i></button>";
                } else if(is_numeric($d)){
                    return "<button class='btn btn-danger btn-sm tblRun'>Stop <i class='fa fa-stop'></i></button>";
                } else {
                    return false;
                }
            }
        ),
    );
     
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraCondition)
    );
}