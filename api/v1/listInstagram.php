<?php
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$array = array();
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun); 
if($_SESSION['user_status']!= 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    $table          = 'tbl_instagram';
    $primaryKey     = 'id_instagram';
    //$joinQuery      = "FROM `{$table}` AS `re` INNER JOIN `tbl_instagram` AS `us` ON (`re`.`id_instagram` = `us`.`id_instagram`)";
    $extraCondition = "id_user='".$id_user."'";
    //$where          = 'tbl_instagram_log.id_instagram=tbl_instagram.id_instagram';
    $columns        = array(
        array(
            'db' => 'id_instagram',
            'dt' => 0,
            'field' => 'id_instagram'
        ),
        array(
            'db' => 'username',
            'dt' => 1,
            'field' => 'username',
            'formatter' => function($d, $row){
                return '<a href="https://instagram.com/'.$d.'">@'.$d.'</a>';
            }
        ),
        array(
            'db' => 'created_at',
            'dt' => 2,
            'field' => 'created_at',
            'formatter' => function($d, $row){
                return date('d M Y H:i:s', $d);
            }
        )
    );
     
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, null, $extraCondition)
    );
}