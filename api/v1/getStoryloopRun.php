<?php
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');
$array = array();
if($_SESSION['user_status'] != 'login'){
    $array['result']    = 0;
    $array['content']   = 'Session error';
} else {
    if($_POST['id_data']){
        $id_data    = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $select     = mysqli_query($conn, "SELECT * FROM `tbl_storyloop_run` WHERE id_sl_run='".$id_data."'");
       	$data 		= mysqli_fetch_assoc($select);
        print_r(json_encode($data));
    }
}