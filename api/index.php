<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'API Developer';
if ($_SESSION['user_status'] == 'login') {
    $id_user = $_SESSION['user_id'];
    $myakun = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
    $myakun = mysqli_fetch_assoc($myakun);
}
?>
<!doctype html>
<html lang="en">
<?php
include('../inc/head.phtml');
?>

<body class="body-bg">
    <?php
    include('../inc/header.phtml');
    ?>
    <main role="main" class="container" style="padding-top: 100px">

        <div class="row">

            <div class="row">
                <div class="col-md-4">
                    <div class="sticky-top">
                        <div class="card border-primary">
                            <div class="card-header bg-primary text-white"><i class="fa fa-info"></i> Info</div>
                            <div class="card-body">
                                <?php
                                if ($_SESSION['user_status'] == 'login') {
                                ?>
                                    <div class="form-group">
                                        <label>Your API Key :</label>
                                        <input type="text" class="form-control" placeholder="Apikey" value="<?= $myakun['apikey']; ?>" disabled>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-info">Please <a href="<?= $config['host']; ?>/account/auth">login or register</a> for create API key</div>
                                <?php } ?>
                            </div>
                        </div><br>
                        <div class="card border-primary">
                            <div class="card-header bg-primary text-white"><i class="fa fa-list"></i> List API</div>
                            <div class="card-body">
                                <?php
                                $tampil = mysqli_query($conn, "SELECT * FROM tbl_api_docs ORDER BY title ASC") or die(mysqli_error($conn));
                                //echo mysqli_num_rows($tampil).' total available API';
                                while ($data = mysqli_fetch_array($tampil, MYSQLI_ASSOC)) { ?>
                                    <li><a href="#<?= str_replace(' ', '-', $data['title']) ?>"><?= $data['title'] ?></a></li>
                                <?php } ?>
                            </div>
                        </div><br>
                    </div>
                </div>
                <div class="col-md-8">
                    <?php
                    $tampil = mysqli_query($conn, "SELECT * FROM tbl_api_docs ORDER BY title ASC") or die(mysqli_error($conn));
                    while ($data = mysqli_fetch_assoc($tampil)) {
                    ?>
                        <div class="card border-primary" id="<?= str_replace(' ', '-', $data['title']) ?>">
                            <div class="card-header bg-primary text-white"><i class="fa fa-random"></i> <?= $data['title'] ?></div>
                            <div class="card-body">
                                <div class="container">
                                    <section>
                                        <div class="form-group">
                                            <label>Api Url</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-link"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?= $config['host']; ?>/api/<?= $data['site'] ?>" disabled="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Method</label>
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-random"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?= $data['action'] ?>" disabled="">
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Parameter</th>
                                                    <th>Required</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $id_api = $data['id_api'];
                                                $tampil2 = mysqli_query($conn, "SELECT * FROM tbl_api_params WHERE id_api = '$id_api' ORDER BY parameter ASC");
                                                while ($data2 = mysqli_fetch_array($tampil2, MYSQLI_ASSOC)) {
                                                ?>
                                                    <tr>
                                                        <td><?= $data2['parameter'] ?></td>
                                                        <?php if ($data2['required'] == 'yes') {
                                                            echo "<td>Required</td>";
                                                        } else {
                                                            echo "<td>Optional</td>";
                                                        } ?>
                                                        <td><?= $data2['description'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <p><strong>Success Response:</strong></p>
                                        <div class="card card-block bg-light card-body">
                                            <pre class="language-json"><?= $data['result'] ?></pre>
                                        </div></br>
                                    </section>
                                </div>
                            </div>
                        </div><br>
                    <?php } ?>
                </div>
            </div>

        </div>

    </main>
    <!-- /.container -->
    <?php
    include('../inc/footer.phtml');
    ?>
    <?php
    include('../inc/foot.phtml');
    ?>
    <script src="<?= $config['host']; ?>/assets/js/siel.js"></script>
</body>

</html>