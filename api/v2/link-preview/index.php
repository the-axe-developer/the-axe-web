<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result 	= array();
$apikey 	= mysqli_real_escape_string($conn, stripslashes(trim($_GET['apikey'])));
$link 		= mysqli_real_escape_string($conn, stripslashes(trim($_GET['link'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if($apikey == null){
	$result['status']	= 'error';
    $result['message']	= 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']	= 'error';
	$result['message']	= 'Invalid API key';
} else if($link == null){
	$result['status']	= 'error';
    $result['message']	= 'Link is required';
} else {
	$url    = 'http://api.linkpreview.net/?key=c716698dfa46b62e1fd69fc79ca8ba5f&q='.$link;
    $exc    = basic_cURL($url, 0, 0, 0, 0, $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $json   = json_decode($exc[1], true);
    if($json['error'] != null){
		$result['status']	= 'error';
		$result['message']	= $json['error'];
	} else {
		$result['status']	= 'ok';
		$result['message']	= 'Welcome to Axe Media API V2';
	}
	$result['data']		= $json;
	mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Link Preview', '$input', '$ip', '$time')");
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>