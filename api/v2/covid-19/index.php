<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$country    = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['country'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if($apikey == null){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else {
    $url    = 'https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total?country='.$country;
    $key    = $config['rapidapi_key'];
    $exc    = basic_cURL($url, 0, 0, 0, array('X-Mashape-Key: '.$key, 'X-RapidAPI-Host: covid-19-coronavirus-statistics.p.rapidapi.com'), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $json   = json_decode($exc[1], true);
    $result['status']   = 'ok';
    $result['message']  = 'Welcome to Axe Media API V2';
    $result['data']     = $json['data'];
    mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'COVID-19 Statistics', '$input', '$ip', '$time')");
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>