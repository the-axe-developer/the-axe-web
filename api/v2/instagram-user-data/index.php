<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$username   = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['username'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if($apikey == null){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else if($username == null){
    $result['status']   = 'error';
    $result['message']  = 'Username required';
} else {
    $request            = proccess(1, $config['ig_useragent_default'], 'users/'.$username.'/usernameinfo/', $config['ig_cookie_default'], 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $data               = json_decode($request[1], true);
    if($data['status'] == 'ok'){
        $result['status']   = 'ok';
        $result['message']  = 'Welcome to Axe Media API V2';
        $result['user']     = $data['user'];
    } else {
        $result['status']   = 'error';
        $result['message']  = $data['message'];
    }
    mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Instagram User Info', '$input', '$ip', '$time')");
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>