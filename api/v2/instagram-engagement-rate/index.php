<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$username   = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['username'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if(empty($apikey)){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
    //var_dump($_REQUEST);
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else if($username == null){
    $result['status']   = 'error';
    $result['message']  = 'Username required';
} else {
    $request            = proccess(1, $config['ig_useragent_default'], 'users/'.$username.'/usernameinfo/', $config['ig_cookie_default'], 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $data               = json_decode($request[1], true);
    $uid                = $data['user']['pk'];
    $priv               = $data['user']['is_private'];
    if(!$uid){
        $result['status']   = 'error';
        $result['message']  = 'User not found';
    } elseif($priv == true){
        $result['status']   = 'error';
        $result['message']  = 'User is private';
    } else {
        $ambilmedia         = proccess(1, $config['ig_useragent_default'], 'feed/user/'.$data['user']['pk'].'/', $config['ig_cookie_default'], 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
        $datamedia          = json_decode($ambilmedia[1], true);
        $limit              = 12;
        $mediacount         = $datamedia['num_results'];
        $like_t             = 0;
        $come_t             = 0;

        if($mediacount >= $limit){
            $limit = $limit;
        } elseif($mediacount <= $limit){
            $limit = $mediacount;
        }

        for($i=0; $i < $limit; $i++){
            $code   = $datamedia['items'][$i]['code'];
            $like_c = $datamedia['items'][$i]['like_count'];
            $come_c = $datamedia['items'][$i]['comment_count'];
            $like_t = $like_t+$like_c;
            $come_t = $come_t+$come_c;
        }

        $total_interaksi        = $like_t+$come_t;
        $follower               = $data['user']['follower_count'];
        $following              = $data['user']['following_count'];
        $average_likes          = round(($like_t/$limit));
        $average_comments       = round(($come_t/$limit));
        $average_interaction    = round(($total_interaksi/$limit));
        $engagement_rate        = number_format((($like_t/$limit)+($come_t/$limit))/$follower*100, 2);
        //echo "((likes total/limit media)+(comment total/limit media))/follower*100<br>";
        $result['status']   = 'ok';
        $result['message']  = 'Welcome to Axe Media API V2';
        $result['data']['pk']                       = $data['user']['pk'];
        $result['data']['username']                 = $data['user']['username'];
        $result['data']['full_name']                = $data['user']['full_name'];
        $result['data']['is_private']               = $data['user']['is_private'];
        $result['data']['is_verified']              = $data['user']['is_verified'];
        $result['data']['profile_pic_url']          = $data['user']['profile_pic_url'];
        $result['data']['hd_profile_pic_versions']  = $data['user']['hd_profile_pic_versions'];
        $result['data']['media_count']              = $data['user']['media_count'];
        $result['data']['follower_count']           = $data['user']['follower_count'];
        $result['data']['following_count']          = $data['user']['following_count'];
        $result['data']['average']['average_likes']         = $average_likes;
        $result['data']['average']['average_comments']      = $average_comments;
        $result['data']['average']['average_interaction']   = $average_interaction;
        $result['data']['engagement']['engagement_rate']    = $engagement_rate;
        $result['data']['engagement']['engagement_unit']    = '%';
        //$result['data']['user']             = $datamedia;
        mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Instagram Engagement Calculator', '$input', '$ip', '$time')");
    }
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>