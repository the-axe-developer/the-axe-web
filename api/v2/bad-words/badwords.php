<?php
class Badwords
{
  private function getBadWords()
  {
    return array_map(function($item){
      return strtolower(trim($item));
    }, explode("\n", file_get_contents(__DIR__ . '/blacklist.txt')));
  }

  public function isDirty($string)
  {
    $words = explode(' ', $string);
    $bad_words = $this->getBadWords();
    foreach ($words as $word) {
      if(in_array(strtolower($word), $bad_words)){
        return 'bad';
      }
    }
    return 'not bad';
  }

  public function strip($string)
  {
    $words = explode(' ', $string);
    $bad_words = $this->getBadWords();
    $new_words = [];
    foreach ($words as $word) {
      if(in_array(strtolower($word), $bad_words)){
        $new_words[] = str_ireplace(['a', 'i', 'u', 'e', 'o', '4', '1', '3', '0'], '*', $word);
      }
      else{
        $new_words[] = $word;
      }
    }
    return implode(' ', $new_words);
  }

}