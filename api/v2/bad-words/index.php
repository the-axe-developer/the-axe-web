<?php
header('Content-Type: application/json');
require_once('badwords.php');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');

use Ramsey\Uuid\Uuid;

$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$text       = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['text'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user       = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid       = Uuid::uuid4();
$input		= json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if ($apikey == null) {
	$result['status']   = 'error';
	$result['message']  = 'API key required';
	$http_code 			= 400;
} else if (mysqli_num_rows($cek) < 1) {
	$result['status']   = 'error';
	$result['message']  = 'Invalid API key';
	$http_code 			= 400;
} else if ($text == null) {
	$result['status']   = 'error';
	$result['message']  = 'Text is required';
	$http_code 			= 400;
} else {
	$http_code 			= 200;
	$result['status']   = 'ok';
	$result['message']  = '';
	$init               = new Badwords;
	$cekbad             = $init->isDirty($text);
	if ($cekbad == 'bad') {
		$badword = true;
	} else {
		$badword = false;
	}
	$stripbad  = $init->strip($text);
	$result['data']['bad']       = $badword;
	$result['data']['strip_bad'] = $stripbad;
	mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Bad Words Checker', '$input', '$ip', '$time')");
}

http_response_code($http_code);
print_r(json_encode($result, JSON_PRETTY_PRINT));
