<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$ip         = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['ip'])));
$port       = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['port'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if($apikey == null){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else if($ip == null){
    $result['status']   = 'error';
    $result['message']  = 'IP address is required';
} else if($port == null){
    $result['status']   = 'error';
    $result['message']  = 'Port is required';
} else {
    $check = @fsockopen($ip, $port, $errno, $error, 10);
    if($check){
        $result['status']           = 'ok';
        $result['message']          = 'Welcome to Axe Media API V2';
        $result['data']['ip']       = $ip;
        $result['data']['port']     = $port;
        $result['data']['status']   = true;
        fclose($con);
    } else {
        $result['status']           = 'ok';
        $result['message']          = 'Can\'t connect to proxy';
        $result['data']['ip']       = $ip;
        $result['data']['port']     = $port;
        $result['data']['status']   = false;
    }
    mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Proxy Check', '$input', '$ip', '$time')");
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>