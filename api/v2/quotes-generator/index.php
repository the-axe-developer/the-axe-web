<?php
header('Content-Type: application/json');
require_once('../../../lib/config.php');
require_once('../../../lib/vendor/autoload.php');
use Ramsey\Uuid\Uuid;
$result 	= array();
$apikey 	= mysqli_real_escape_string($conn, stripslashes(trim($_GET['apikey'])));
$count 		= mysqli_real_escape_string($conn, stripslashes(trim($_GET['count'])));
$count 		= empty($count) ? '1' : $count;
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
$user 	    = mysqli_fetch_assoc($cek);
$userid     = $user['id_user'];
$uuid 	    = Uuid::uuid4();
$input 	    = json_encode($_REQUEST);
$time       = time();
$ip         = ip();
if($apikey == null){
	$result['status']	= 'error';
    $result['message']	= 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']	= 'error';
	$result['message']	= 'Invalid API key';
} else if(!is_numeric($count)){
	$result['status']	= 'error';
	$result['message']	= 'Count must numeric';
} else {
	$url    = 'https://andruxnet-random-famous-quotes.p.rapidapi.com/?cat=famous&count='.$count;
    $key    = $config['rapidapi_key'];
    $exc    = basic_cURL($url, 0, 0, 0, array('X-Mashape-Key: '.$key, 'X-RapidAPI-Host: andruxnet-random-famous-quotes.p.rapidapi.com'), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $json   = json_decode($exc[1], true);
	$result['status']	= 'ok';
	$result['message']	= 'Welcome to Axe Media API V2';
	$result['data']		= $json;
	mysqli_query($conn, "INSERT INTO `tbl_api_history` (`id_history`, `id_user`, `name`, `data`, `ip_address`, `created_at`) VALUES ('$uuid', '$userid', 'Quotes Generator', '$input', '$ip', '$time')");
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>