<?php
header('Content-Type: application/json');
require('../../lib/config.php');
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$cek        = mysqli_query($conn, "SELECT * FROM tbl_user WHERE apikey='".$apikey."'");
if($apikey == null){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else {
    $user_data          = mysqli_fetch_assoc($cek);
    $result['status']   = 'ok';
    $result['message']  = 'Welcome to Axe Media API V2';
    $result['data']     = $user_data;
    unset($result['data']['password']);
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>