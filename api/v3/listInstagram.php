<?php
header('Content-Type: application/json');
require('../../lib/config.php');
$result     = array();
$apikey     = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['apikey'])));
$id_data    = mysqli_real_escape_string($conn, stripslashes(trim($_REQUEST['id'])));
$cek        = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE `apikey`='$apikey'");
if($apikey == null){
    $result['status']   = 'error';
    $result['message']  = 'API key required';
} else if(mysqli_num_rows($cek) < 1){
    $result['status']   = 'error';
    $result['message']  = 'Invalid API key';
} else {
    $user_data          = mysqli_fetch_assoc($cek);
    $id_user            = $user_data['id_user'];
    if($id_data){
        $insta_query = mysqli_query($conn, "SELECT * FROM `tbl_instagram` WHERE `id_user`='$id_user' AND `id_instagram`='$id_data'");
    } else {
        $insta_query = mysqli_query($conn, "SELECT * FROM `tbl_instagram` WHERE `id_user`='$id_user'");
    }
    for($rows           = array(); $row = mysqli_fetch_assoc($insta_query); $rows[] = $row);
    if($rows == null){
        $result['status']   = 'error';
        $result['message']  = 'Data not found';
    } else {
        $result['status']   = 'ok';
        $result['message']  = 'Welcome to '.$config['name'].' API V3';
        $result['data']     = $rows;
    }
}
print_r(json_encode($result, JSON_PRETTY_PRINT));
?>