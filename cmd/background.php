<?php
require('../lib/config.php');
require('../lib/vendor/autoload.php');
use Cocur\BackgroundProcess\BackgroundProcess;
$process = new BackgroundProcess('php test.php');
$process->run();

echo sprintf('Crunching numbers in process %d', $process->getPid());
while ($process->isRunning()) {
    echo '.';
    sleep(1);
}
echo "\nDone.\n";
?>