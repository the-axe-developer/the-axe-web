<?php
session_start();
require('../../lib/config.php');
if ($_GET['limit']) {
    $limit  = $_GET['limit'];
    $latest = mysqli_query($conn, "SELECT * FROM `tbl_dataset` ORDER BY `created_at` DESC LIMIT $limit");
    for ($rows = array(); $row = mysqli_fetch_assoc($latest); $rows[] = $row);
    for ($i = 0; $i < count($rows); $i++) {
        $rows[$i]['date']   = date('d M Y H:i:s', $rows[$i]['created_at']);
        $rows[$i]['change'] = ($rows[$i]['price'] - $rows[$i + 1]['price']);
        if ($rows[$i]['price'] == $rows[$i]['change']) {
            $rows[$i]['change'] = 0;
        }
    }
    $data = $rows;
} else {
    $latest = mysqli_query($conn, "SELECT * FROM `tbl_dataset` ORDER BY `created_at` DESC");
    $latest = mysqli_fetch_array($latest);

    $data['data']   = $latest['price'];
    $data['label']  = 'Market Price';
}

echo json_encode($data);
