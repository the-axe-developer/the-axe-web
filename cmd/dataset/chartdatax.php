<?php
session_start();
require('../../lib/config.php');
$today          = date('d-m-Y H:i:s');
$harimundur1    = date("d-m-Y H:i:s", mktime(date("H"), date("i"), date("s") - 1, date("m"), date("d"), date("Y")));
$harimundur2    = date("d-m-Y H:i:s", mktime(date("H"), date("i"), date("s") - 2, date("m"), date("d"), date("Y")));
$harimundur3    = date("d-m-Y H:i:s", mktime(date("H"), date("i"), date("s") - 3, date("m"), date("d"), date("Y")));
$harimundur4    = date("d-m-Y H:i:s", mktime(date("H"), date("i"), date("s") - 4, date("m"), date("d"), date("Y")));
$harimundur5    = date("d-m-Y H:i:s", mktime(date("H"), date("i"), date("s") - 5, date("m"), date("d"), date("Y")));
$hitungtoday    = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$today'"))['price'];
$hitungmundur1  = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$harimundur1'"))['price'];
$hitungmundur2  = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$harimundur2'"))['price'];
$hitungmundur3  = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$harimundur3'"))['price'];
$hitungmundur4  = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$harimundur4'"))['price'];
$hitungmundur5  = mysqli_fetch_array(mysqli_query($conn, "SELECT `price`, FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s') AS `date_format` FROM `tbl_dataset` WHERE FROM_UNIXTIME(tbl_dataset.created_at, '%d-%m-%Y %H:%i:%s')='$harimundur5'"))['price'];

$data['data'] = [$hitungmundur5, $hitungmundur4, $hitungmundur3, $hitungmundur2, $hitungmundur1, $hitungtoday];
$data['label'] = [$harimundur5, $harimundur4, $harimundur3, $harimundur2, $harimundur1, $today];

echo json_encode($data);
