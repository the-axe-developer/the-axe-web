<?php
session_start();
require('../../lib/config.php');
$config['pagename'] = 'Data Set';
$id_user    = $_SESSION['user_id'];
$myakun     = mysqli_query($conn, "SELECT * FROM `tbl_user` WHERE id_user='$id_user'");
$myakun     = mysqli_fetch_assoc($myakun);
?>
<!doctype html>
<html lang="en">
<?php
include('../../inc/head.phtml');
?>

<body class="body-bg">
    <?php
    include('../../inc/header.phtml');
    ?>
    <main role="main" class="container" style="padding-top: 100px">

        <div class="row">
            <div class="col-lg-12">
                <?php
                include('../../inc/navigation.phtml');
                ?>
            </div>

            <div class="col-lg-12">

                <div class="card border-primary mb-2">
                    <div class="card-header bg-primary text-white"><i class="fa fa-chart-line"></i> Chart API Request</div>
                    <div class="card-body">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-history"></i> Request History</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="List-Data" class="display table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody id="Table-Body">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Date</th>
                                        <th>Price</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div><br>
            </div>

        </div>

    </main>
    <!-- /.container -->
    <?php
    include('../../inc/footer.phtml');
    ?>
    <?php
    include('../../inc/foot.phtml');
    ?>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-streaming@1.8.0"></script>
    <script type="text/javascript">
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        function randomScalingFactor() {
            return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
        }

        function onRefresh(chart) {
            console.log(Date.now());
            $.ajax({
                url: "<?= $config['host']; ?>/cmd/dataset/chart",
                timeout: false,
                type: 'GET',
                dataType: 'JSON',
                success: function(hasil) {
                    console.log(hasil);
                    chart.config.data.datasets.forEach(function(dataset) {
                        dataset.data.push({
                            x: Date.now(),
                            y: hasil.data
                        });
                    });
                },
                error: function(a, b, c) {
                    chart.config.data.datasets.forEach(function(dataset) {
                        dataset.data.push({
                            x: Date.now(),
                            y: randomScalingFactor()
                        });
                    });
                }
            });
        }

        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                datasets: [{
                    label: 'TAXE (The Axe Media Tbk)',
                    backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: chartColors.blue,
                    fill: false,
                    lineTension: 0,
                    borderDash: [8, 4],
                    data: []
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Market Price'
                },
                scales: {
                    xAxes: [{
                        type: 'realtime',
                        realtime: {
                            duration: 20000,
                            refresh: 5000,
                            delay: 0,
                            onRefresh: onRefresh
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'value'
                        }
                    }]
                },
                tooltips: {
                    mode: 'nearest',
                    intersect: false
                },
                hover: {
                    mode: 'nearest',
                    intersect: false
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myChart = new Chart(ctx, config);
        };
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#Navigation-Select").change(function() {
                window.location.href = '<?= $config['host']; ?>/' + $(this).val();
            });

            (function load_data_poll() {
                const id_data = '<?= $_GET['id']; ?>';
                setTimeout(function() {
                    $.ajax({
                        url: "<?= $config['host']; ?>/cmd/dataset/chart?limit=10",
                        type: 'GET',
                        dataType: 'JSON',
                        complete: load_data_poll,
                        success: function(hasil) {
                            console.log(hasil);
                            $('#Table-Body').html('');
                            for (const item of hasil) {
                                var table = $('#Table-Body').html();
                                var rows = '<tr><td>' + item.date + '</td><td>' + item.price + '</td></tr>';
                                $("#Table-Body").html(table + '' + rows);
                            }

                        }
                    });
                }, 3000)
            })();

            //load_data_poll();

        });
    </script>
</body>

</html>