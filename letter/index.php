<?php
session_start();
require('../lib/config.php');
$config['pagename'] = 'Letter from Nico';
$config['description'] = 'Ada surat untuk kamu dari nico xixixi';
$config['og_image'] = 'https://www.brandeps.com/icon-download/L/Love-Letter-icon-vector-01.svg';
?>
<!doctype html>
<html lang="en">
    <head>
        <title><?=$config['pagename'];?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta http-equiv="Cache-Control" content="public" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="googlebot" content="index,follow"/>
        <meta name="robots" content="index,follow"/>
        <meta name="description" content="<?=$config['description'];?>">
        <meta name="keywords" content="<?=$config['keyword'];?>" />
        <meta name="author" content="<?=$config['author'];?>">
        <link rel="icon" href="<?=$config['favicon'];?>">

        <!-- Theme Color URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
        <meta name="theme-color" content="#593196"/>
        <!-- URL Theme Color untuk Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#593196" />
        <!-- URL Theme Color untuk iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#593196" />
        <!-- End Theme Color -->

        <!-- Google / Search Engine Tags -->
        <meta itemprop="name" content="<?=$config['pagename'];?>">
        <meta itemprop="description" content="<?=$config['description'];?>">
        <meta itemprop="image" content="<?=$config['og_image'];?>">
        
        <!-- Facebook Meta Tags -->
        <meta property="og:url" content="<?=$config['host'];?>">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?=$config['pagename'];?>">
        <meta property="og:description" content="<?=$config['description'];?>">
        <meta property="og:image" content="<?=$config['og_image'];?>">
        
        <!-- Twitter Meta Tags -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="<?=$config['pagename'];?>">
        <meta name="twitter:description" content="<?=$config['description'];?>">
        <meta name="twitter:image" content="<?=$config['og_image'];?>">
        <!-- Meta Tags Generated via http://heymeta.com -->

        <!-- Bootstrap core CSS -->
        <link href="<?=$config['host'];?>/assets/css/bootstrap-pulse.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?=$config['host'];?>/assets/css/custom.css" rel="stylesheet">
        <!-- Fontawesome CSS -->
        <link href="<?=$config['host_admin'];?>/plugins/fontawesome-free/css/all.min.css" rel="stylesheet">
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Noto Sans' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Catamaran&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Hind:wght@500&display=swap" rel="stylesheet">
        <!-- Bootstrap Toggle -->
        <link href="<?=$config['host'];?>/assets/css/bootstrap4-toggle.min.css" rel="stylesheet">
        <!-- Pace CSS -->
        <link href="<?=$config['host'];?>/assets/css/pace.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                background-color: #f9f9f9;
                font-family: 'Noto Sans';
            }

            .typedtext {
                font-size: 20px;
                font-family: 'Hind';
                z
            }

            .title {
                font-family: 'Josefin Sans';
            }
        </style>
    </head>
    <body class="body-bg">
        <?php
        //nclude('../inc/header.phtml');
        ?>
        <main role="main" class="container" style="padding-top: 100px">

            <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                    <div class="content">
                        <h1 class="title text-center">Letter from Nico <i class="fa fa-heart" style="color: red"></i></h1><br>
                        <div>
                            <div id="typedtext" class="typedtext"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
            </div>

        </main>
        <!-- /.container -->
        <?php
        include('../inc/footer.phtml');
        ?>
        <?php
        include('../inc/foot.phtml');
        ?>
        <script>
            var aText = new Array(
                "Hai mutual twitter aku yang baca ini, yang lama atau yang baru kenal, makasih udah mau temenan sama aku, jadi saksi kegabutan aku selama pandemi ini wkwk", 
                "Seneng banget bisa sedeket ini sama kalian padahal kita belum pernah saling ketemu satu sama lain ya. Sukses terus buat kalian yaa!!",
                "Love <3"
            );
            var iSpeed = 100; // time delay of print out
            var iIndex = 0; // start printing array at this posision
            var iArrLength = aText[0].length; // the length of the text array
            var iScrollAt = 20; // start scrolling up at this many lines
            var iTextPos = 0; // initialise text position
            var sContents = ''; // initialise contents variable
            var iRow; // initialise current row
             
            function typewriter(){
                sContents =  ' ';
                iRow = Math.max(0, iIndex-iScrollAt);
                var destination = document.getElementById("typedtext");
             
                while ( iRow < iIndex ) {
                    sContents += aText[iRow++] + '<br/><br/>';
                }
                destination.innerHTML = sContents + aText[iIndex].substring(0, iTextPos) + "|";
                if ( iTextPos++ == iArrLength ) {
                    iTextPos = 0;
                    iIndex++;
                    if ( iIndex != aText.length ) {
                        iArrLength = aText[iIndex].length;
                        setTimeout("typewriter()", 500);
                    }
                } else {
                    setTimeout("typewriter()", iSpeed);
                }
            }
            $(document).ready(function(){
                typewriter();
            });
        </script>
    </body>
</html>