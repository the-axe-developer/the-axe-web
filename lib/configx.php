<?php
date_default_timezone_set('Asia/Jakarta');

//Database Configuration
$sql_details['user']	= 'theaxene_website';
$sql_details['pass']	= 'hahalol123!';
$sql_details['db']		= 'theaxene_website';
$sql_details['host']	= 'theaxe.net';
$conn                 	= mysqli_connect($sql_details['host'], $sql_details['user'], $sql_details['pass'], $sql_details['db']);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

//Website Configuration
$select                     = mysqli_query($conn, "SELECT * FROM tbl_config WHERE id='1'");
$config                     = mysqli_fetch_array($select);
$config['host']             = 'http://localhost/axemedia-new';
$config['host_admin']       = $config['host'].'/admin-page';
$config['rapidapi_key']     = 'a45a9b2343msh8f366a0da882b30p18e395jsn0ec0b3d8ac18';
$config['axemedia_key']     = 'EJiZoMAfb4VOAflZgXIbVusA59z1umt7';
$config['error']            = 0; // 0 = False and 1 = True
$config['mt']               = 0; // 0 = False and 1 = True
$config['proxy_ip']         = 0;
$config['proxy_auth']       = 0;
$config['proxy_socks5']     = 0;

// Include
include('function.class.php');
include('igfunction.class.php');
include('ssp.class.php');
include('OAuth.class.php');
include('twitteroauth.class.php');

//Maintenance Redirect & Debug
if ($config['error'] == 0) {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
} elseif ($config['error'] == 1) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}
if ($config['mt'] == 1) {
    if (!strpos($_SERVER['SCRIPT_FILENAME'], 'maintenance.php')) {
        header("Location: ".$config['host']."/maintenance");
        exit();
    }
}
