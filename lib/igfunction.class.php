<?php
set_time_limit(0);
ignore_user_abort(1);
function trim_replace($data)
{
    $a = trim($data);
    $a = str_replace('<', '', $a);
    $a = str_replace('>', '', $a);
    $a = str_replace('"', '', $a);
    $a = str_replace('`', '', $a);
    $a = str_replace('-', '', $a);
    return $a;
}

function GetTimeDiff($timestamp)
{
    $how_log_ago = '';
    $seconds     = $timestamp - time();
    $minutes     = (int) ($seconds / 60);
    $hours       = (int) ($minutes / 60);
    $days        = (int) ($hours / 24);
    if ($days >= 1) {
        $how_log_ago = $days . ' hari';
    } else if ($hours >= 1) {
        $how_log_ago = $hours . ' jam';
    } else if ($minutes >= 1) {
        $how_log_ago = $minutes . ' menit';
    } else {
        $how_log_ago = $seconds . ' detik';
    }
    return $how_log_ago;
}

function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
{
    if (isset($_SERVER['HTTP_HOST'])) {
        $http     = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
        $hostname = $_SERVER['HTTP_HOST'];
        $dir      = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
        $core     = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
        $core     = $core[0];
        $tmplt    = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
        $end      = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
        $base_url = sprintf($tmplt, $http, $hostname, $end);
    } else
        $base_url = 'http://localhost/';
    if ($parse) {
        $base_url = parse_url($base_url);
        if (isset($base_url['path']))
            if ($base_url['path'] == '/')
                $base_url['path'] = '';
    }
    return $base_url;
}


function proccess($ighost, $useragent, $url, $cookie = 0, $data = 0, $httpheader = array(), $proxy = 0, $userpwd = 0, $is_socks5 = 0)
{
    $url = $ighost ? 'https://i.instagram.com/api/v1/' . $url : $url;
    $ch  = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    if ($proxy)
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
    if ($userpwd)
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $userpwd);
    if ($is_socks5)
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    if ($httpheader)
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    if ($cookie)
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    if ($data):
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    endif;
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch);
    if (!$httpcode)
        return false;
    else {
        $header = substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
        $body   = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
        curl_close($ch);
        return array(
            $header,
            $body
        );
    }
}

function proccess_v2($ighost, $useragent, $url, $cookie = 0, $data = 0, $httpheader = array(), $proxy = 0, $userpwd = 0, $is_socks5 = 0)
{
    $url = $ighost ? 'https://i.instagram.com/api/v2/' . $url : $url;
    $ch  = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    if ($proxy)
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
    if ($userpwd)
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $userpwd);
    if ($is_socks5)
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    if ($httpheader)
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    if ($cookie)
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    if ($data):
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    endif;
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch);
    if (!$httpcode)
        return false;
    else {
        $header = substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
        $body   = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
        curl_close($ch);
        return array(
            $header,
            $body
        );
    }
}

function cekpoint($url, $data, $csrf, $cookies, $ua){
    $a = curl_init();
    curl_setopt($a, CURLOPT_URL, $url);
    curl_setopt($a, CURLOPT_USERAGENT, $ua);
    curl_setopt($a, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($a, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($a, CURLOPT_HEADER, 1);
    curl_setopt($a, CURLOPT_COOKIE, $cookies);
    if($data){
    curl_setopt($a, CURLOPT_POST, 1);   
    curl_setopt($a, CURLOPT_POSTFIELDS, $data);
    }
    if($csrf){
    curl_setopt($a, CURLOPT_HTTPHEADER, array(
            'Connection: keep-alive',
            'Proxy-Connection: keep-alive',
            'Accept-Language: en-US,en',
            'x-csrftoken: '.$csrf,
            'x-instagram-ajax: 1',
            'Referer: '.$url,
            'x-requested-with: XMLHttpRequest',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    ));
    }
    $b = curl_exec($a);
    return $b;
}

function generate_useragent($sign_version = '133.0.0.32.120')
{
    $resolusi = array(
        '1080x1776',
        '1080x1920',
        '720x1280'
    );
    $versi    = array(
        'GT-N7000',
        'SM-N9000',
        'GT-I9220',
        'GT-I9100'
    );
    $dpi      = array(
        '120',
        '160',
        '320',
        '240'
    );
    $ver      = $versi[array_rand($versi)];
    return 'Instagram ' . $sign_version . ' Android (' . mt_rand(10, 11) . '/' . mt_rand(1, 3) . '.' . mt_rand(3, 5) . '.' . mt_rand(0, 5) . '; ' . $dpi[array_rand($dpi)] . '; ' . $resolusi[array_rand($resolusi)] . '; samsung; ' . $ver . '; ' . $ver . '; smdkc210; en_US)';
}

function hook($data)
{
    return 'ig_sig_key_version=4&signed_body=' . hash_hmac('sha256', $data, '5d406b6939d4fb10d3edb4ac0247d495b697543d3f53195deb269ec016a67911') . '.' . urlencode($data);
}

function ava_hook($bound, $csrf, $file_url)
{
    $eol  = "\r\n";
    $body = '';
    $body .= '--' . $bound . $eol;
    $body .= 'Content-Disposition: form-data; name="_csrftoken"' . $eol . $eol;
    $body .= $csrf . $eol;
    $body .= '--' . $bound . $eol;
    $body .= 'Content-Disposition: form-data; name="profile_pic"; filename="profile_pic"' . $eol;
    $body .= 'Content-Type: application/octet-stream' . $eol;
    $body .= 'Content-Transfer-Encoding: binary' . $eol . $eol;
    $body .= file_get_contents($file_url) . $eol;
    $body .= '--' . $bound . '--' . $eol . $eol;
    return $body;
}

function generate_device_id()
{
    return 'android-' . md5(rand(1000, 9999)) . rand(2, 9);
}

function generate_guid($tipe = 0)
{
    $guid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    return $tipe ? $guid : str_replace('-', '', $guid);
}

function unicode_decode($str)
{
    return preg_replace_callback("/\\\u([0-9a-f]{4})/i", create_function('$matches', 'return html_entity_decode(\'&#x\'.$matches[1].\';\', ENT_QUOTES, \'UTF-8\');'), $str);
}

function hash_code($string)
{
    $result = 0;
    for ($i = 0, $len = strlen($string); $i < $len; ++$i) {
        $result = (-$result + ($result << 5) + ord($string[$i])) & 0xFFFFFFFF;
    }
    if (PHP_INT_SIZE > 4) {
        if ($result > 0x7FFFFFFF) {
            $result -= 0x100000000;
        } elseif ($result < -0x80000000) {
            $result += 0x100000000;
        }
    }

    return $result;
}

function reorder_by_hash_code($data)
{
    $hashCodes = [];
    foreach ($data as $key => $value) {
        $hashCodes[$key] = hash_code($key);
    }

    uksort($data, function ($a, $b) use ($hashCodes) {
        $a = $hashCodes[$a];
        $b = $hashCodes[$b];
        if ($a < $b) {
            return -1;
        } elseif ($a > $b) {
            return 1;
        } else {
            return 0;
        }
    });

    return $data;
}

function download_by_URL($url, $dir = './')
{
    $get        = basic_cURL($url)[1];
    $filename   = explode('?', basename($url))[0];
    $savefile   = $dir.$filename;
    if(file_put_contents($savefile, $get)){
        return $savefile;
    } else {
        return false;
    }
}

function get_user_id($username)
{
    global $config;
    $data   = proccess(1, $config['ig_useragent_default'], 'users/'.$username.'/usernameinfo/', $config['ig_cookie_default'], 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $json   = json_decode($data[1], true);
    $array  = array();
    $uid    = $json['user']['pk'];
    $priv   = $json['user']['is_private'];
    if($json['status'] == 'ok'){
        if(!$uid){
            $array['result']    = 0;
            $array['content']   = 'user_not_found';
        } else {
            $array['result']    = 1;
            $array['content']   = 'yes';
            $array['id']        = $uid;
            $array['private']   = $priv;
        }
    } else {
        $array['result']    = 0;
        $array['content']   = $json['message'];
    }

    return $array;
}

function get_media_id($target){
    global $config;
    $data   = basic_cURL('https://api.instagram.com/oembed/?url='.$target, getUserAgent(), 0, 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
    $json   = json_decode($data[1], true);
    $mid    = $json['media_id'];
    if(!$mid){
        $array['result']    = 0;
        $array['content']   = 'post_not_found';
        $status_error       = true;
    } else {
        $array['result']    = 1;
        $array['content']   = 'yes';
        $array['id']        = $mid;
    }
    return $array;
}

function get_comment_id($url, $target)
{
    global $config;
    $shortcode  = explode('p/', $url);
    $shortcode  = explode('/', $shortcode[1]);
    $targetnya  = $target;
    $next       = false;
    $next_id    = 0;
    do {
        $payload        = '{"shortcode":"'.$shortcode[0].'","first":100,"after":"'.$next_id.'"}';
        $baseurl        = 'https://www.instagram.com/graphql/query/?query_hash=33ba35852cb50da46f5b5e889df7d159&variables='.$payload;
        $get            = basic_cURL($baseurl, getUserAgent(), $config['ig_cookie_default'], 0, array(), $config['proxy_ip'], $config['proxy_auth'], $config['proxy_socks5']);
        $data           = json_decode($get[1], true);
        if($data['status'] == 'ok'){
            if($data['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page'] == true){
                $next       = true;
                $next_id    = $data['data']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'];
            } else {
                $next       = false;
            }
            for($i=0;$i <= count($data['data']['shortcode_media']['edge_media_to_comment']['edges']);$i++){
                $jem    = $data['data']['shortcode_media']['edge_media_to_comment']['edges'][$i]['node']['owner']['username'];
                if($jem == $targetnya){
                    $pknya  = $data['data']['shortcode_media']['edge_media_to_comment']['edges'][$i]['node']['id'];
                    $uidnya = $data['data']['shortcode_media']['edge_media_to_comment']['edges'][$i]['node']['owner']['username'];
                    $next   = false;
                } else {
                    $array['result']        = 0;
                    $array['content']       = 'not_found';
                }
            }
        } else {
            $array['result']        = 0;
            $array['content']       = 'media_not_found';
        }
    } while($next == true);
    if($pknya){
        $array['result']        = 1;
        $array['content']       = 'yes';
        $array['comment_id']    = $pknya;
        $array['user_id']       = $uidnya;
    }

    return $array;
}

function getuid($username)
{
    $url      = "https://www.instagram.com/" . $username;
    $html     = file_get_contents($url);
    $arr      = explode('window._sharedData = ', $html);
    $arr      = explode(';</script>', $arr[1]);
    $obj      = json_decode($arr[0], true);
    $id       = $obj['entry_data']['ProfilePage'][0]['graphql']['user']['id'];
    
    return $id;
}

function getmediaid($url)
{
    $getid   = file_get_contents("https://api.instagram.com/oembed/?url=".$url);
    $json1   = json_decode($getid);
    $mediaid = $json1->media_id;
    if($mediaid){
        return $mediaid;
    } else {
        return false;
    }
}