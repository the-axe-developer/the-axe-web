<?php
date_default_timezone_set('Asia/Jakarta');

// Database configuration
$sql_details['user']    = 'thea_web2';
$sql_details['pass']    = 'Hahalol123!';
$sql_details['db']      = 'thea_web2';
$sql_details['host']    = '139.59.106.229';
$conn = mysqli_connect($sql_details['host'], $sql_details['user'], $sql_details['pass'], $sql_details['db']);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

// Website configuration
$select                 = mysqli_query($conn, "SELECT * FROM `tbl_config` WHERE `id`='1'");
$config                 = mysqli_fetch_array($select);
$config['host']         = ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) ? 'http://the-axe-web.test' : 'https://theaxe.site';
$config['host_links']   = ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) ? 'http://localhost/the-axe-links' : 'https://links.theaxe.net';
$config['host_admin']   = $config['host'] . '/admin-page';
$config['rapidapi_key'] = 'a45a9b2343msh8f366a0da882b30p18e395jsn0ec0b3d8ac18';
$config['axemedia_key'] = 'EJiZoMAfb4VOAflZgXIbVusA59z1umt7';
$config['error']        = 0; // 0 = False and 1 = True
$config['mt']           = 0; // 0 = False and 1 = True
$config['proxy_ip']     = 0;
$config['proxy_auth']   = 0;
$config['proxy_socks5'] = 0;

// Include
include('function.class.php');
include('igfunction.class.php');
include('ssp.class.php');
include('OAuth.class.php');
include('twitteroauth.class.php');

// Maintenance redirect & debug
if ($config['error'] == 0) {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
} elseif ($config['error'] == 1) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

if ($config['mt'] == 1) {
    if (!strpos($_SERVER['SCRIPT_FILENAME'], 'maintenance.php')) {
        header("Location: " . $config['host'] . "/maintenance");
        exit();
    }
}
