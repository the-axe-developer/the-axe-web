<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'].' - Admin Control';
if($_SESSION['admin_status']!= 'login'){
    header('Location:'.$config['host'].'/admin-page/login');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../inc/admin-head.phtml'); ?>
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <?php include('../inc/admin-header.phtml'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">List Tools</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                    	<div class="form-group">
                                    		<a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Add Data</a>
                                    	</div>
                                    	<div class="table-responsive">
	                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
										        <thead>
										            <tr>
										            	<th>Row</th>
                                                        <th>ID Instagram</th>
										                <th>Username</th>
										                <th>Password</th>
										                <th>Status</th>
										                <th>Action</th>
										            </tr>
										        </thead>
										        <tfoot>
										            <tr>
										            	<th>Row</th>
                                                        <th>ID Instagram</th>
                                                        <th>Username</th>
                                                        <th>Password</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
										            </tr>
										        </tfoot>
										    </table>
										</div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
	                    <div class="modal-dialog" role="document">
				        	<div class="modal-content">
				            	<div class="modal-header">
				              		<h4 class="modal-title">Edit Tools</h4>
				              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                		<span aria-hidden="true">&times;</span>
				              		</button>
				            	</div>
				            	<div class="modal-body">
				            		<form method="POST" action="<?=$config['host'];?>/api/v1/admin/editTools" id="Edit-Data-Form">
				            			<input type="hidden" id="id_tools" name="id_tools">
                                        <div class="form-group">
                                            <label>Tools Name</label>
                                            <input type="text" class="form-control" id="tools_name" name="name" placeholder="Nama Tools">
                                        </div>
                                        <div class="form-group">
                                            <label>Page/Directory Tools</label>
                                            <input type="text" class="form-control" id="tools_site" name="site" placeholder="Site Tools">
                                        </div>
                                        <div class="form-group">
                                            <label>Tools Description</label>
                                            <textarea class="form-control" id="tools_description" name="ket" placeholder="Keterangan Tools"></textarea>
                                            </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                            	<option value="" id="tools_status">-- Select Status --</option>
                                                <option value="on">ON</option>
                                                <option value="off">OFF</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="button_id" type="submit"><i class="fa fa-save"></i> Save</button>
                                        </div>
                                    </form>
				            	</div>
				          	</div>
				          <!-- /.modal-content -->
				        </div>
				        <!-- /.modal-dialog -->
			      	</div>
                    <!-- /.modal -->
					<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
	                    <div class="modal-dialog" role="document">
				        	<div class="modal-content">
				            	<div class="modal-header">
				              		<h4 class="modal-title">Add Tools</h4>
				              		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                		<span aria-hidden="true">&times;</span>
				              		</button>
				            	</div>
				            	<div class="modal-body">
				            		<form method="POST" action="<?=$config['host'];?>/api/v1/admin/addTools" id="Add-Data-Form">
                                        <div class="form-group">
                                            <label>Tools Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Nama Tools">
                                        </div>
                                        <div class="form-group">
                                            <label>Page/Directory Tools</label>
                                            <input type="text" class="form-control" name="site" placeholder="Site Tools">
                                        </div>
                                        <div class="form-group">
                                            <label>Tools Description</label>
                                            <textarea class="form-control" name="ket" placeholder="Keterangan Tools"></textarea>
                                            </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option value="on">ON</option>
                                                <option value="off">OFF</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Save</button>
                                            <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                        </div>
                                    </form>
				            	</div>
				          	</div>
				          <!-- /.modal-content -->
				        </div>
				        <!-- /.modal-dialog -->
			      	</div>
			      <!-- /.modal -->			      
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <?php include('../inc/admin-footer.phtml'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->
        <?php include('../inc/admin-foot.phtml'); ?>
        <!-- SweetAlert Plugin JS -->
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

            	//Ambil Data
			    var table = $('#List-Data').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax": "<?=$config['host'];?>/api/v1/admin/listInstagram",
			        "columnDefs": [ {
			            "targets": -1,
			            "data": null,
			            "defaultContent": "<button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button>"
			        }]
			    });

			    //Tambahkan Data
			    $("form#Add-Data-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            	table.ajax.reload(null, false);
                            	$('#modalAdd').modal('hide');
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

			    //Tampilkan Form Ubah Data
				$('#List-Data tbody').on('click', '.tblEdit', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var id_tools = data[0];
	                //swal("Peringatan!", "Tanggal tidak boleh kosong.", "warning");
	                $.ajax({
	                    type : "POST",
	                    url  : "<?=$config['host'];?>/api/v1/admin/getTools",
	                    data : {id:id_tools},
	                    dataType : "JSON",
	                    success: function(result){
	                        // tampilkan modal ubah data transaksi
	                        $('#modalEdit').modal('show');
	                        // tampilkan data transaksi
	                        $('#id_tools').val(result.id);
	                        $('#tools_name').val(result.name);
	                        $('#tools_description').val(result.description);
	                        $('#tools_site').val(result.page_name);
	                        $('#tools_status').val(result.status);
	                    }
	                });
	            });

	            //Simpan ubah data
	            $("form#Edit-Data-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                            	swal("Success!", ""+hasil.content+"", "success");
                            	table.ajax.reload(null, false);
                            	$('#modalEdit').modal('hide');
                            } else
                            	swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                //Hapus Data
				$('#List-Data tbody').on('click', '.tblDelete', function (){
	                var data = table.row($(this).parents('tr')).data();
	                var nama_tools = data[1];
	                var id_tools = data[0];
	                swal({
			            title: "Are you sure?",
			            text: "You will not be able to recover this data!",
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#DD6B55",
			            confirmButtonText: "Yes, delete it!",
			            closeOnConfirm: false
			        },
			        function(isConfirm){
			        	if(isConfirm){
				            $.ajax({
			                    type : "POST",
			                    url  : "<?=$config['host'];?>/api/v1/admin/deleteTools",
			                    data : {id:id_tools},
			                    dataType : "JSON",
			                    success: function(hasil){
		                            if(hasil.result){
		                            	swal("Success", ""+hasil.content+"", "success");
		                            	table.ajax.reload(null, false);
		                            } else
		                            	swal("Failed", ""+hasil.content+"", "error");
		                        }
			                });
			          	} else {
			                swal("Cancelled", "Your imaginary file is safe :)", "error");
			        	} 
			       })
	            });

			} );
        </script>
    </body>
</html>