<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Admin Control';
if ($_SESSION['admin_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-header.phtml'); ?>

        <div class="content-wrapper">

            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active">Manage User</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">List User</h3>
                                </div>

                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Add Data</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">User ID</th>
                                                    <th class="text-nowrap">Email</th>
                                                    <th class="text-nowrap">First Name</th>
                                                    <th class="text-nowrap">Last Name</th>
                                                    <th class="text-nowrap">API Key</th>
                                                    <th class="text-nowrap">Register Date</th>
                                                    <th class="text-nowrap">Last Login</th>
                                                    <th class="text-nowrap">IP Address</th>
                                                    <th class="text-nowrap">Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">User ID</th>
                                                    <th class="text-nowrap">Email</th>
                                                    <th class="text-nowrap">First Name</th>
                                                    <th class="text-nowrap">Last Name</th>
                                                    <th class="text-nowrap">API Key</th>
                                                    <th class="text-nowrap">Register Date</th>
                                                    <th class="text-nowrap">Last Login</th>
                                                    <th class="text-nowrap">IP Address</th>
                                                    <th class="text-nowrap">Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalView" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View API Parameter</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-nowrap">Parameter</th>
                                                <th class="text-nowrap">Required</th>
                                                <th class="text-nowrap">Description</th>
                                            </tr>
                                        </thead>
                                        <tbody id="Api-Param">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/v1/admin/editUser" id="Edit-Data-Form">
                                    <input type="hidden" id="id_data" name="id_data">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" id="email" class="form-control" disabled="">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" name="password" class="form-control" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirmation Password</label>
                                        <input type="text" name="confirm_password" class="form-control" value="">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/v1/admin/addUser" id="Add-Data-Form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="f_name" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="l_name" placeholder="Last Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" name="c_password" placeholder="Confirm Password">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Save</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            </div>

        </div>

        <!-- Main Footer -->
        <?php include('../inc/admin-footer.phtml'); ?>
    </div>

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-foot.phtml'); ?>
    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            //Ambil Datas
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/v1/admin/listUser",
                "columnDefs": [{
                        "targets": 0,
                        "visible": false
                    }, {
                        "targets": '_all',
                        "className": 'text-nowrap'
                    },
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<div class='d-flex justify-content-between'><div class='col mb-2'><button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button></div><div class='col mb-2'><button class='btn btn-success btn-sm tblView'><i class='fa fa-eye'></i></button></div><div class='col mb-2'><button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></div></div>"
                    },
                ],
                "order": [
                    [5, "desc"]
                ]
            });

            //Tambahkan Data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            //Tampilkan Form Ubah Data
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                //swal("Peringatan!", "Tanggal tidak boleh kosong.", "warning");
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/v1/admin/getUser",
                    data: {
                        id: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        // tampilkan modal ubah data transaksi
                        $('#modalEdit').modal('show');
                        // tampilkan data transaksi
                        $('#id_data').val(result.id_user);
                        $('#email').val(result.email);
                    }
                });
            });

            //Tampilkan Form Ubah Data ? KEMEM
            $('#List-Data tbody').on('click', '.tblView', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                //swal("Peringatan!", "Tanggal tidak boleh kosong.", "warning");
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/v1/admin/getApiParamV2",
                    data: {
                        id: id_data
                    },
                    dataType: "HTML",
                    success: function(result) {
                        $('#modalView').modal('show');
                        $('#Api-Param').html(result);
                    }
                });
            });

            //Simpan ubah data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            //Hapus Data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/v1/admin/deleteUser",
                                data: {
                                    id_data: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.result) {
                                        swal("Success", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>