<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Admin Control';
if ($_SESSION['admin_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <?php
            $query = mysqli_query($conn, "
            SELECT 'total_user' AS category, COUNT(*) AS total FROM `tbl_user`
            UNION ALL
            SELECT 'total_tools' AS category, COUNT(*) AS total FROM `tbl_tools`
            UNION ALL
            SELECT 'total_api' AS category, COUNT(*) AS total FROM `tbl_api_docs`
            UNION ALL
            SELECT 'total_req' AS category, COUNT(*) AS total FROM `tbl_api_history`
            ");

            $results = array();

            while ($row = mysqli_fetch_assoc($query)) {
                $results[$row['category']] = $row['total'];
            }

            $total_user = $results['total_user'];
            $total_tools = $results['total_tools'];
            $total_api = $results['total_api'];
            $total_req = $results['total_req'];

            ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3><?= $total_user; ?></h3>
                                    <p>Total User</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-user" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3><?= $total_tools; ?></h3>
                                    <p>Total Tools</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cogs"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-tools" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3><?= $total_api; ?></h3>
                                    <p>Total API</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-random"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-api-docs" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3><?= $total_req; ?></h3>
                                    <p>Total API Request</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/monitoring-api" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../inc/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-foot.phtml'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Edit-Config-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            $("#result_submit").html('<div class="alert alert-success">' + hasil.content + '</div>');
                            status();
                        } else
                            $("#result_submit").html('<div class="alert alert-danger">' + hasil.content + '</div>');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Edit-ReChapta-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            $("#result_submit_2").html('<div class="alert alert-success">' + hasil.content + '</div>');
                            status();
                        } else
                            $("#result_submit_2").html('<div class="alert alert-danger">' + hasil.content + '</div>');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit_2").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id_2").html('Loading..');
                        $("#result_submit_2").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>