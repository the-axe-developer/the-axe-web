<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'].' - Admin Control';
if($_SESSION['admin_status']!= 'login'){
    header('Location:'.$config['host'].'/admin-page/login');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../inc/admin-head.phtml'); ?>
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <?php include('../inc/admin-header.phtml'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Website Configuration</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $select     = mysqli_query($conn, "SELECT * FROM tbl_config WHERE id='1'");
                                        $configx    = mysqli_fetch_array($select);
                                        ?>
                                        <div id="result_submit"></div>
                                        <form method="POST" action="<?=$config['host'];?>/api/v1/admin/editConfig" id="Edit-Config-Form">
                                            <div class="form-group">
                                                <label>Domain / Host</label>
                                                <input type="text" class="form-control" name="host" value="<?=$configx['host'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Site Name</label>
                                                <input type="text" class="form-control" name="name" value="<?=$configx['name'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Site Title</label>
                                                <input type="text" class="form-control" name="title" value="<?=$configx['title'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" name="desc" value="<?=$configx['description'];?>"><?=$config['description'];?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Keyword</label>
                                                <textarea class="form-control" name="keyword" value="<?=$configx['keyword'];?>"><?=$config['keyword'];?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Author</label>
                                                <input type="text" class="form-control" name="author" value="<?=$configx['author'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>OG Image</label>
                                                <input type="text" class="form-control" name="ogimg" value="<?=$configx['og_image'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Favicon</label>
                                                <input type="text" class="form-control" name="favicon" value="<?=$configx['favicon'];?>">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id" type="submit"><i class="fa fa-save"></i> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">ReChapta Key</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $select     = mysqli_query($conn, "SELECT * FROM tbl_config WHERE id='1'");
                                        $configx    = mysqli_fetch_array($select);
                                        ?>
                                        <div id="result_submit_2"></div>
                                        <form method="POST" action="<?=$config['host'];?>/api/v1/admin/editRechapta" id="Edit-ReChapta-Form">
                                            <div class="form-group">
                                                <label>Site Key</label>
                                                <input type="text" class="form-control" name="site_key" value="<?=$configx['recaptcha_site_key'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Secret Key</label>
                                                <input type="text" class="form-control" name="secret_key" value="<?=$configx['recaptcha_secret_key'];?>">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Proxy Config</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $select     = mysqli_query($conn, "SELECT * FROM tbl_config WHERE id='1'");
                                        $configx    = mysqli_fetch_array($select);
                                        ?>
                                        <form method="POST" action="<?=$config['host'];?>/api/v1/admin/editConfigProxy" id="Edit-Proxy-Form">
                                            <div class="form-group">
                                                <label>Proxy IP</label>
                                                <input type="text" class="form-control" name="proxy_ip" value="<?=$configx['proxy_ip'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Proxy Auth</label>
                                                <input type="text" class="form-control" name="proxy_auth" value="<?=$configx['proxy_auth'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Is Socks 5</label>
                                                <select name="proxy_socks5" class="form-control">
                                                    <option value="<?=$configx['proxy_socks5'];?>">-- Is Socks 5 (<?=$configx['proxy_socks5'];?>) --</option>
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id_4" type="submit"><i class="fa fa-save"></i> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Instagram Data</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $select     = mysqli_query($conn, "SELECT * FROM tbl_config WHERE id='1'");
                                        $configx    = mysqli_fetch_array($select);
                                        ?>
                                        <form method="POST" action="<?=$config['host'];?>/api/v1/admin/editConfigInstagram" id="Edit-Instagram-Form">
                                            <div class="form-group">
                                                <label>Cookie</label>
                                                <textarea class="form-control" name="ig_cookie" value="<?=$configx['ig_cookie_default'];?>"><?=$configx['ig_cookie_default'];?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Useragent</label>
                                                <input type="text" class="form-control" name="ig_useragent" value="<?=$configx['ig_useragent_default'];?>">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id_5" type="submit"><i class="fa fa-save"></i> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <?php include('../inc/admin-footer.phtml'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->
        <?php include('../inc/admin-foot.phtml'); ?>
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Edit-Config-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('<i class="fa fa-save"></i> Save');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Edit-ReChapta-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                            $("#result_submit_2").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id_2").html('Loading..');
                            $("#result_submit_2").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Edit-Proxy-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_4").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_4").html('<i class="fa fa-save"></i> Save');
                            swal("Failed!", ""+c+"", "error");
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id_4").html('Loading..');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });

                $("form#Edit-Instagram-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_5").html('<i class="fa fa-save"></i> Save');
                            if(hasil.result){
                                swal("Success!", ""+hasil.content+"", "success");
                            } else
                                swal("Failed!", ""+hasil.content+"", "error");
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id_5").html('<i class="fa fa-save"></i> Save');
                            swal("Failed!", ""+c+"", "error");
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id_5").html('Loading..');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>