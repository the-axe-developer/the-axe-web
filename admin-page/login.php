<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'].' - Admin Control';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../inc/admin-head.phtml'); ?>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?=$config['host_admin'];?>"><b><?=$config['name'];?></b></a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>
                    <div id="result_submit"></div>
                    <form action="<?=$config['host'];?>/api/v1/admin/login" method="post" id="Login-Form">
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">
                                    Remember Me
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block" id="button_id">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->
        <?php include('../inc/admin-foot.phtml'); ?>
        <script src="https://www.google.com/recaptcha/api.js?render=<?=$config['recaptcha_site_key'];?>"></script>
        <script type="text/javascript">
            grecaptcha.ready(function() {
                grecaptcha.execute('<?=$config['recaptcha_site_key'];?>', {action: 'homepage'}).then(function(token){
                    $('#Login-Form').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("form#Login-Form").submit(function() {
                    var pdata = $(this).serialize();
                    var purl = $(this).attr('action');
                    $.ajax({
                        url: purl,
                        data: pdata,
                        timeout: false,
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(hasil){
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('Login');
                            if(hasil.result){
                                $("#result_submit").html('<div class="alert alert-success">'+hasil.content+'</div>');
                                status();
                            } else
                                $("#result_submit").html('<div class="alert alert-danger">'+hasil.content+'</div>');
                          },
                        error: function(a, b, c) {
                            $("input").removeAttr("disabled", "disabled");
                            $("button").removeAttr("disabled", "disabled");
                            $("#button_id").html('Login');
                            $("#result_submit").html(c);
                        },
                        beforeSend: function() {
                            $("input").attr("disabled", "disabled");
                            $("#button_id").html('Loading..');
                            $("#result_submit").html('');
                            $("button").attr("disabled", "disabled");
                        }
                    });
                    return false
                });
            });
        </script>
    </body>
</html>