<?php
session_start();
require('../lib/config.php');
$config['title'] = $config['name'].' - Admin Control';
if($_SESSION['admin_status']!= 'login'){
    header('Location:'.$config['host'].'/admin-page/login');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../inc/admin-head.phtml'); ?>
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <?php include('../inc/admin-header.phtml'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Top API</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $query = mysqli_query($conn, "SELECT `name`, COUNT(*) AS `name_count` FROM `tbl_api_history` GROUP BY `name` ORDER BY `name_count` DESC") or die(mysqli_error($conn));
                                        ?>
                                        <div class="table-responsive">
                                            <table class="display table table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>API Name</th>
                                                        <th>Total Request</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(mysqli_num_rows($query) > 0){
                                                while($datax = mysqli_fetch_assoc($query)){ ?>
                                                    <tr>
                                                        <td><?=$datax['name'];?></td>
                                                        <td><?=$datax['name_count'];?> Request</td>
                                                    </tr>
                                                <?php }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="2" class="text-center">No data available</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Top User</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php
                                        $query = mysqli_query($conn, "SELECT `tbl_user`.`email`, `tbl_api_history`.`id_user`, COUNT(*) AS `req_count` FROM `tbl_api_history` INNER JOIN `tbl_user` ON `tbl_api_history`.`id_user`=`tbl_user`.`id_user` GROUP BY `tbl_api_history`.`id_user` ORDER BY `req_count` DESC") or die(mysqli_error($conn));
                                        ?>
                                        <div class="table-responsive">
                                            <table class="display table table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>API Name</th>
                                                        <th>Total Request</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(mysqli_num_rows($query) > 0){
                                                while($datax = mysqli_fetch_assoc($query)){ ?>
                                                    <tr>
                                                        <td><?=$datax['email'];?></td>
                                                        <td><?=$datax['req_count'];?> Request</td>
                                                    </tr>
                                                <?php }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="2" class="text-center">No data available</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Request History</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                    	<div class="table-responsive">
	                                        <table id="List-Tools" class="display table table-bordered" style="width:100%">
										        <thead>
										            <tr>
										            	<th>Date</th>
										                <th>Email</th>
                                                        <th>API Name</th>
										                <th>IP Address</th>
										                <th>Data</th>
										            </tr>
										        </thead>
										        <tfoot>
										            <tr>
										            	<th>Date</th>
                                                        <th>Email</th>
                                                        <th>API Name</th>
                                                        <th>IP Address</th>
                                                        <th>Data</th>
										            </tr>
										        </tfoot>
										    </table>
										</div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>		      
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <?php include('../inc/admin-footer.phtml'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->
        <?php include('../inc/admin-foot.phtml'); ?>
        <!-- SweetAlert Plugin JS -->
        <script type="text/javascript" src="<?=$config['host'];?>/assets/js/sweetalert.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

            	//Ambil Data
			    var table = $('#List-Tools').DataTable({
			        "processing": true,
			        "serverSide": true,
			        "ajax": "<?=$config['host'];?>/api/v1/admin/listApiRequest",
                    "order": [[0, 'desc']],
			        "columnDefs": [{}]
			    });

			} );
        </script>
    </body>
</html>